package onlycollect;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapOutputCollector;
import org.apache.hadoop.mapred.MapOutputCollector.Context;
import org.apache.hadoop.mapred.MapTask;
import org.apache.hadoop.mapred.Task.TaskReporter;
import org.apache.hadoop.mapred.TaskUmbilicalProtocol;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.map.TokenCounterMapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.reduce.IntSumReducer;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;
import org.apache.hadoop.util.Progress;

import pluggable.MapOutputHeapWithKeyPrefix;
import pluggable.MapOutputHeapWithMetadataHeap;

public class TestHelper {

    private String INPUT;
    private String OUTPUT;
    private int bufferSize;

    public TestHelper() {

    }

    public FreshKeyLineRecordReader getLineReader(Configuration conf)
            throws IOException {
        File testFile = new File(getInput());
        FileSplit split = new FileSplit(new Path(testFile.getAbsolutePath()),
                0, testFile.length(), null);
        FreshKeyLineRecordReader lineReader = new FreshKeyLineRecordReader();
        lineReader.initialize(split, new TaskAttemptContextImpl(conf,
                new TaskAttemptID()));
        return lineReader;
    }

    public Context getContext(Configuration conf, Job job)
            throws ClassNotFoundException, InstantiationException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException,
            SecurityException, NoSuchFieldException {

        MapTask mapTask = new MapTask();
        mapTask.setConf(conf);

        Progress sortPhase = new Progress();
        sortPhase.addPhase("sort", 0.333f);

        Field f = mapTask.getClass().getDeclaredField("sortPhase");
        f.setAccessible(true);
        f.set(mapTask, sortPhase);

        /*Class<?> umbClass = Class
                .forName("org.apache.hadoop.mapred.TestMapProgress$FakeUmbilical");
        Constructor<?> umbConstructor = umbClass.getDeclaredConstructors()[0];
        umbConstructor.setAccessible(true);
        TaskUmbilicalProtocol umbilical = (TaskUmbilicalProtocol) umbConstructor
                .newInstance();*/

        Class<?> taskClass = Class.forName("org.apache.hadoop.mapred.Task");

        Method startReporter = taskClass.getDeclaredMethod("startReporter",
                TaskUmbilicalProtocol.class);
        startReporter.setAccessible(true);
        Object o = startReporter.invoke(mapTask, new Object[]{null});

        return new MapOutputCollector.Context(mapTask,
                (JobConf) job.getConfiguration(), (TaskReporter) o);
    }

    @SuppressWarnings("deprecation")
    public Job getJob(Configuration conf, String bufferName) throws IOException {
        Job job = new Job(conf, "word count");
        job.setJarByClass(JunitTestPluggableMapOutputBuffers.class);

        job.setMapperClass(TokenCounterMapper.class);
        job.setReducerClass(IntSumReducer.class);
        job.setNumReduceTasks(4);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path(getInput()));
        FileOutputFormat.setOutputPath(job, new Path(getOutput() + "_" + bufferName));

        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(new Path(getOutput()))) {
            hdfs.delete(new Path(getOutput()), true);
        }
        return job;
    }

    public Configuration getConfiguration(MapOutputCollector<Text, IntWritable> buffer) {
        Configuration conf = new Configuration();

        if (buffer.getClass().equals(MapOutputHeapWithKeyPrefix.class) || buffer.getClass().equals(MapOutputHeapWithMetadataHeap.class))
            conf.set("io.serializations", "io.serialization.WritableSerializationWithZeroEndingText");
        conf.setInt("mapreduce.task.io.sort.mb", getBufferSize());
        conf.set("fs.defaultFS", "file:///");
        conf.set("mapreduce.cluster.local.dir", getOutput());
        return conf;
    }

    public Configuration getConfiguration() {
        Configuration conf = new Configuration();
        conf.setInt("mapreduce.task.io.sort.mb", getBufferSize());
        conf.set("fs.defaultFS", "file:///");
        conf.set("mapreduce.cluster.local.dir", getOutput());
        return conf;
    }

    public static <K, V> int getPartition(K key, V value, int numReduceTasks) {
        return (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
    }

    public static void showCurrentExperimentArguments(String string,
                                                      String string2, String algorithm, int ioSortMegabytes) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println("========================");
        /*
         * System.out.println("Input file: " + string);
		 * System.out.println("Output dir: " + string2);
		 */
        System.out.println("Algorithm : " + algorithm);
        System.out.println("Buffer    : " + ioSortMegabytes);
        System.out.println("Time      : " + dateFormat.format(new Date()));
        System.out.println("========================");
    }

    static void writeData(SortedMap<String, Long> runs) throws IOException {

        if (runs.size() > 1)
            System.out.println("Spill Nro:" + (runs.size() - 1));
        else
            System.out.println("Spill Nro: 1");
        /*
         * for (Entry<String, Long> file : runs.entrySet()) {
		 * System.out.println("" + "#" + file.getKey() + ": " +
		 * file.getValue()); }
		 */
    }

    static SortedMap<String, Long> gatherData(String path) {
        path += "output/";

        SortedMap<String, Long> runs = new TreeMap<String, Long>();

        for (String file : new File(path).list()) {
            if (!file.contains("file.out.index")) {
                File f = new File(path + "/" + file);
                runs.put(f.getName(), f.length());
            }
        }

        return runs;

    }

    public String getInput() {
        return INPUT;
    }

    public void setInput(String iNPUT) {
        INPUT = iNPUT;
    }

    public String getOutput() {
        return OUTPUT;
    }

    public void setOutput(String oUTPUT) {
        OUTPUT = oUTPUT;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }
}
