package onlycollect;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapOutputCollector;
import org.apache.hadoop.mapreduce.Job;
import org.junit.Test;

import pluggable.MapOutputHeap;
import pluggable.MapOutputHeapWithKeyPrefix;
import pluggable.MapOutputHeapWithMetadataHeap;
import pluggable.MapOutputQueue;

public class JunitTestPluggableMapOutputBuffers {

	private static final String INPUT = "/media/Data/input/256MxB";
	private static final String OUTPUT = "/media/Data/out/tests/queue/";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testQueue() throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException, InterruptedException,
			NoSuchFieldException {

		TestHelper test = new TestHelper();
		test.setInput(INPUT);
		test.setOutput(OUTPUT);

		Configuration conf = test.getConfiguration();
		Job job = test.getJob(conf, "queue");
		FreshKeyLineRecordReader lineReader = test.getLineReader(conf);
		MapOutputCollector.Context context = test.getContext(conf, job);

		MapOutputQueue buffer = new MapOutputQueue<Text, IntWritable>();
		buffer.init(context);

		try {
			while (lineReader.nextKeyValue()) {
				int partition = TestHelper.getPartition(
						lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), job.getNumReduceTasks());

				buffer.collect(lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), partition);
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

		buffer.flush();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testHeap() throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException, InterruptedException,
			NoSuchFieldException {

		TestHelper test = new TestHelper();
		test.setInput(INPUT);
		test.setOutput(OUTPUT);

		Configuration conf = test.getConfiguration();
		Job job = test.getJob(conf, "heap");
		FreshKeyLineRecordReader lineReader = test.getLineReader(conf);
		MapOutputCollector.Context context = test.getContext(conf, job);

		MapOutputHeap buffer = new MapOutputHeap<Text, IntWritable>();
		buffer.init(context);

		try {
			while (lineReader.nextKeyValue()) {
				int partition = TestHelper.getPartition(
						lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), job.getNumReduceTasks());

				buffer.collect(lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), partition);
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

		buffer.flush();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testHeapWithKeyPrefix() throws IOException,
			ClassNotFoundException, InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException,
			SecurityException, InterruptedException, NoSuchFieldException {

		TestHelper test = new TestHelper();
		test.setInput(INPUT);
		test.setOutput(OUTPUT);

		Configuration conf = test.getConfiguration();

		conf.set("io.serializations",
				"io.serialization.WritableSerializationWithZeroEndingText");

		Job job = test.getJob(conf, "keyprefix");
		FreshKeyLineRecordReader lineReader = test.getLineReader(conf);
		MapOutputCollector.Context context = test.getContext(conf, job);

		MapOutputHeapWithKeyPrefix buffer = new MapOutputHeapWithKeyPrefix<Text, IntWritable>();
		buffer.init(context);

		try {
			while (lineReader.nextKeyValue()) {
				int partition = TestHelper.getPartition(
						lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), job.getNumReduceTasks());

				buffer.collect(lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), partition);
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

		buffer.flush();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testHeapWithKeyPrefixAndBinaryMetadataHeap()
			throws IOException, ClassNotFoundException, InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException,
			SecurityException, InterruptedException, NoSuchFieldException {

		TestHelper test = new TestHelper();
		test.setInput(INPUT);
		test.setOutput(OUTPUT);

		Configuration conf = test.getConfiguration();
		Job job = test.getJob(conf, "metadata");

		conf.set("io.serializations",
				"io.serialization.WritableSerializationWithZeroEndingText");

		FreshKeyLineRecordReader lineReader = test.getLineReader(conf);
		MapOutputCollector.Context context = test.getContext(conf, job);

		MapOutputHeapWithMetadataHeap buffer = new MapOutputHeapWithMetadataHeap<Text, IntWritable>();
		buffer.init(context);

		try {
			while (lineReader.nextKeyValue()) {
				int partition = TestHelper.getPartition(
						lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), job.getNumReduceTasks());

				buffer.collect(lineReader.getCurrentKey(),
						lineReader.getCurrentValue(), partition);
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

		buffer.flush();
	}

}
