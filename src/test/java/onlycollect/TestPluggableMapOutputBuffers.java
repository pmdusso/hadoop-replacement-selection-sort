package onlycollect;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapOutputCollector;
import org.apache.hadoop.mapred.MapTask.MapOutputBuffer;
import org.apache.hadoop.mapreduce.Job;

import org.apache.log4j.PropertyConfigurator;
import pluggable.MapOutputHeap;
import pluggable.MapOutputHeapWithKeyPrefix;
import pluggable.MapOutputHeapWithMetadataHeap;
import pluggable.MapOutputQueue;

public class TestPluggableMapOutputBuffers {

    private static final String MAP_OUTPUT_QUEUE = "MapOutputQueue";
    private static final String MAP_OUTPUT_HEAP = "MapOutputHeap";
    private static final String MAP_OUTPUT_HEAP_WITH_KEY_PREFIX = "MapOutputHeapWithKeyPrefix";
    private static final String MAP_OUTPUT_HEAP_WITH_KEY_PREFIX_AND_HEAP = "MapOutputHeapWithMetadataHeap";

    private static TestHelper test;

    public static void main(String[] args) throws IOException,
            ClassNotFoundException, InstantiationException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException,
            SecurityException, InterruptedException, NoSuchFieldException {

        PropertyConfigurator.configure("log4j.properties");

        if (args.length != 4) {
            System.err
                    .println("Usage: <in> <out> <bufferSize> <collectorAlgorithm>");
            System.exit(2);
        }

        test = new TestHelper();
        test.setInput(args[0]);
        test.setOutput(args[1]);
        test.setBufferSize(Integer.parseInt(args[2]));

        switch (args[3]) {
            case MAP_OUTPUT_QUEUE:
                getJobDone(new MapOutputQueue<Text, IntWritable>(), "queue");
                break;
            case MAP_OUTPUT_HEAP:
                getJobDone(new MapOutputHeap<Text, IntWritable>(), "heap");
                break;
            case MAP_OUTPUT_HEAP_WITH_KEY_PREFIX:
                getJobDone(new MapOutputHeapWithKeyPrefix<Text, IntWritable>(),
                        "keyprefix");
                break;
            case MAP_OUTPUT_HEAP_WITH_KEY_PREFIX_AND_HEAP:
                getJobDone(new MapOutputHeapWithMetadataHeap<Text, IntWritable>(),
                        "metaheap");
                break;
            default:
                getJobDone(new MapOutputBuffer<Text, IntWritable>(), "original");
                break;
        }

    }

    private static void getJobDone(
            MapOutputCollector<Text, IntWritable> buffer, String bufferNickname)
            throws IOException, ClassNotFoundException, InstantiationException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException,
            SecurityException, InterruptedException, NoSuchFieldException {

        TestHelper.showCurrentExperimentArguments(test.getInput(), test.getOutput(), bufferNickname, test.getBufferSize());

        Configuration conf = test.getConfiguration(buffer);
        Job job = test.getJob(conf, bufferNickname);
        FreshKeyLineRecordReader lineReader = test.getLineReader(conf);
        MapOutputCollector.Context context = test.getContext(conf, job);

        buffer.init(context);

        while (lineReader.nextKeyValue()) {
            int partition = TestHelper.getPartition(lineReader.getCurrentKey(),
                    lineReader.getCurrentValue(), job.getNumReduceTasks());

            buffer.collect(lineReader.getCurrentKey(),
                    lineReader.getCurrentValue(), partition);
        }

        buffer.flush();

        if (!buffer.getClass().equals(MapOutputBuffer.class)) {

            TestHelper.writeData(TestHelper.gatherData(test.getOutput()));
        }
    }
}
