/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.serialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.hadoop.classification.InterfaceAudience;
import org.apache.hadoop.classification.InterfaceStability;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.Serialization;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.hadoop.util.ReflectionUtils;

/**
 * A {@link Serialization} for {@link Writable}s that delegates to
 * {@link Writable#write(java.io.DataOutput)} and
 * {@link Writable#readFields(java.io.DataInput)}.
 */
@InterfaceAudience.Public
@InterfaceStability.Evolving
public class WritableSerializationWithZeroEndingText extends Configured
		implements Serialization<Writable> {
	public static final int DEFAULT_BYTE_ARRAY_SIZE = 256;

	static class WritableDeserializerWithZeroEnding extends Configured
			implements Deserializer<Writable> {

		private Class<?> writableClass;
		private DataInputStream dataIn;
		byte[] newBytes = new byte[DEFAULT_BYTE_ARRAY_SIZE];

		public WritableDeserializerWithZeroEnding(Configuration conf, Class<?> c) {
			setConf(conf);
			this.writableClass = c;
		}

		@Override
		public void open(InputStream in) {
			if (in instanceof DataInputStream) {
				dataIn = (DataInputStream) in;
			} else {
				dataIn = new DataInputStream(in);
			}
		}

		@Override
		public Writable deserialize(Writable w) throws IOException {
			Writable writable;
			if (w == null) {
				writable = (Writable) ReflectionUtils.newInstance(
						writableClass, getConf());
			} else {
				writable = w;
			}
			if (writable instanceof Text) {
				((Text) writable).set(newBytes, 0, readTextFields());
			} else {
				writable.readFields(dataIn);
			}
			return writable;
		}

		/**
		 * We subtract one from the length because we do not wish the trailing
		 * zero byte in the string.
		 * */
		private int readTextFields() {
			int newLength = 0;
			try {

				newLength = WritableUtils.readVInt(dataIn) - 1;
				dataIn.readFully(newBytes, 0, newLength);

			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			return newLength;
		}

		/*
		 * private int readTextFields() { int length = 0; byte b = 0; try { int
		 * s = WritableUtils.readVInt(dataIn); int r = dataIn.skipBytes(s);
		 * assert s == r;
		 * 
		 * while ((b = dataIn.readByte()) != '\0') { if (length ==
		 * newBytes.length) { newBytes = doubleNewBytesLength(newBytes); }
		 * 
		 * newBytes[length] = b; length++; } } catch (Exception e) {
		 * System.out.println("Something went wrong..." + e); } return length; }
		 */

		@Override
		public void close() throws IOException {
			dataIn.close();
		}
	}

	/*
	 * private static byte[] doubleNewBytesLength(byte[] newBytes) { int
	 * actualLenght = newBytes.length; byte[] temp = new byte[actualLenght];
	 * System.arraycopy(newBytes, 0, temp, 0, actualLenght); newBytes = new
	 * byte[actualLenght * 2]; System.arraycopy(temp, 0, newBytes, 0,
	 * actualLenght); return newBytes; }
	 */
	static class WritableSerializerWithZeroEnding extends Configured implements
			Serializer<Writable> {

		private DataOutputStream dataOut;
		private byte[] zero;

		@Override
		public void open(OutputStream out) {
			if (out instanceof DataOutputStream) {
				dataOut = (DataOutputStream) out;
			} else {
				dataOut = new DataOutputStream(out);
			}
			zero = new byte[1];
			zero[0] = '\0';
		}

		@Override
		public void serialize(Writable w) throws IOException {

			if (w instanceof Text) {
				// byte[] bytes = ((Text) w).getBytes();
				((Text) w).append(zero, 0, 1);
				w.write(dataOut);
				/*
				 * dataOut.write(bytes, 0, bytes.length); // Write a trailing
				 * byte zero WritableUtils.writeVInt(dataOut, 0);
				 */
			} else {
				w.write(dataOut);
			}
		}

		@Override
		public void close() throws IOException {
			dataOut.close();
		}

	}

	@InterfaceAudience.Private
	@Override
	public boolean accept(Class<?> c) {
		return Writable.class.isAssignableFrom(c);
	}

	@InterfaceAudience.Private
	@Override
	public Serializer<Writable> getSerializer(Class<Writable> c) {
		return new WritableSerializerWithZeroEnding();
	}

	@InterfaceAudience.Private
	@Override
	public Deserializer<Writable> getDeserializer(Class<Writable> c) {
		return new WritableDeserializerWithZeroEnding(getConf(), c);
	}

}
