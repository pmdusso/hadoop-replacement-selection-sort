package io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalDirAllocator;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.mapred.Counters.Counter;
import org.apache.hadoop.mapred.IFile.Writer;
import org.apache.hadoop.mapred.IndexRecord;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapOutputFile;
import org.apache.hadoop.mapred.SpillRecord;

public class AsyncRunOutputWriter<K, V> {

	private static final Log LOG = LogFactory.getLog(AsyncRunOutputWriter.class
			.getName());

	private static final int INTEGER_SIZE = Integer.SIZE / 8;

	private static class WriterThread<K, V> extends Thread {

		private static final int INDEX_CACHE_MEMORY_LIMIT = 1024 * 1024;

		private RingBuffer rb;
		private ByteBuffer bb;
		private Writer<K, V> writer;
		private SpillRecord spillRec;

		private FSDataOutputStream outStream;
		private FileSystem rfs;
		private MapOutputFile mapOutputFile;
		private JobConf job;
		private Counter spilledRecordsCounter;
		private final Class<K> keyClass;
		private final Class<V> valClass;
		private final CompressionCodec codec;
		private DataInputBuffer keyBuf;
		private DataInputBuffer valueBuf;
		private ArrayList<SpillRecord> indexCacheList;

		private int totalIndexRecords = 0;
		private int partitions = -1;
		private int currentBlockPos = 0;
		private int writerCurrPart = -1;
		private int writerCurrRun = -1;
		private int recordsConsumed = 0;
		private int totalIndexCacheMemory = 0;
		private long segmentStart = -1;

		private byte[] currentBlock;

		public WriterThread(RingBuffer rb, JobConf job, FileSystem rfs,
				Class<K> keyClass, Class<V> valClass, CompressionCodec codec,
				MapOutputFile mapOutputFile, Counter spilledRecordsCounter) {
			this.rb = rb;
			this.job = job;
			this.partitions = job.getNumReduceTasks();
			this.rfs = rfs;
			this.keyClass = keyClass;
			this.valClass = valClass;
			this.codec = codec;
			this.mapOutputFile = mapOutputFile;
			this.spilledRecordsCounter = spilledRecordsCounter;
			this.spillRec = new SpillRecord(this.partitions);
			this.keyBuf = new DataInputBuffer();
			this.valueBuf = new DataInputBuffer();
			this.indexCacheList = new ArrayList<SpillRecord>();

		}

		private boolean newBlock() throws InterruptedException, IOException {
			if (currentBlock != null) {
				rb.consumeRelease();
			}

			currentBlock = rb.consumeRequest();

			if (currentBlock == null) {
				assert writer != null;
				closeWriter(writerCurrRun + 1);
				return false;
			}

			bb = ByteBuffer.wrap(currentBlock);
			int newRun = bb.getInt();
			int newPart = bb.getInt();

			if (newRun != writerCurrRun || newPart != writerCurrPart) {
				if (writer != null) {
					closeWriter(newRun);
				}

				if (newRun != writerCurrRun) {

					Path p = mapOutputFile.getSpillFileForWrite(newRun,
							LocalDirAllocator.SIZE_UNKNOWN);
					outStream = rfs.create(p);
					if (totalIndexRecords < newPart) {

						writeDummyPartitionIndex(newPart - 1);
					} else
						// First set
						segmentStart = outStream.getPos();

					if (writerCurrRun >= 0)
						LOG.info("Created a new file to spill");
				}

				writer = new Writer<K, V>(job, outStream, keyClass, valClass,
						codec, spilledRecordsCounter);

				writerCurrRun = newRun;
				writerCurrPart = newPart;
			}
			currentBlockPos = 8;

			return true;
		}

		private void closeWriter(int newRun) throws IOException {

			writer.close();
			addIndexRecordToSpillRecords(writerCurrPart);

			if (newRun != writerCurrRun) {
				// This must be called when ALL PARTITIONS have been written.
				// The index cache list has one SpillRec object for each spilled
				// file.
				if (writerCurrPart < partitions - 1) {
					for (int i = writerCurrPart + 1; i < partitions; i++) {
						writeDummyPartitionIndex(i);
					}
				}

				LOG.info(String
						.format("Run number %d closed with %d records written. Starting new run file.",
								writerCurrRun, recordsConsumed));

				addSpillRecordToIndexCacheList();

				spillRec = new SpillRecord(job.getNumReduceTasks());
				totalIndexRecords = 0;
			}

		}

		private void writeDummyPartitionIndex(int partition) throws IOException {
			try {

				segmentStart = outStream.getPos();
				writer = new Writer<K, V>(job, outStream, keyClass, valClass,
						codec, spilledRecordsCounter);
				writer.close();
				addIndexRecordToSpillRecords(partition);

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		/**
		 * For each partition, we must have one index record value. For example,
		 * if we are writing the index record of partition 3, we must have
		 * already written for partitions 0, 1 and 2 (== 3). This way, if the
		 * number of index records in the spillRec object is smaller than the
		 * current partition value, we must create dummy entries for the ones
		 * which are missing.
		 * */
		private void addIndexRecordToSpillRecords(int partition)
				throws IOException {
			IndexRecord rec = new IndexRecord();
			rec.startOffset = segmentStart;
			rec.rawLength = writer.getRawLength();
			rec.partLength = writer.getCompressedLength();

			spillRec.putIndex(rec, partition);
			totalIndexRecords++;

			System.out.println("Partition=" + partition + "("
					+ spillRec.getIndex(partition).startOffset + ","
					+ spillRec.getIndex(partition).rawLength + ", "
					+ spillRec.getIndex(partition).partLength + ")");
		}

		private void addSpillRecordToIndexCacheList() throws IOException {
			if (totalIndexCacheMemory >= INDEX_CACHE_MEMORY_LIMIT) {
				// create spill index file
				Path indexFilename = mapOutputFile.getSpillIndexFileForWrite(
						writerCurrRun, job.getNumReduceTasks() * 24);
				spillRec.writeToFile(indexFilename, job);
			} else {
				this.indexCacheList.add(spillRec);
				totalIndexCacheMemory += spillRec.size() * 24;
			}
		}

		private boolean check() throws InterruptedException, IOException {
			if (currentBlock == null) {
				return newBlock();
			}
			// see if block ended
			int available = currentBlock.length - currentBlockPos;
			if (available <= INTEGER_SIZE) {
				return newBlock();
			}
			if (bb.getInt(currentBlockPos) == 0) {
				return newBlock();
			}
			return true;
		}

		@Override
		public void run() {

			while (true) {
				int keyLen = 0;
				try {
					// get key
					if (!check()) {
						LOG.info("Async writer retired.");
						return;
					}

					keyLen = bb.getInt(currentBlockPos);
					currentBlockPos += INTEGER_SIZE;
					keyBuf.reset(currentBlock, currentBlockPos, keyLen);
					currentBlockPos += keyLen;

					// get value
					int valueLen = bb.getInt(currentBlockPos);
					currentBlockPos += INTEGER_SIZE;
					valueBuf.reset(currentBlock, currentBlockPos, valueLen);
					currentBlockPos += valueLen;

					this.recordsConsumed++;
					// System.out.println("Consumed");
					writer.append(keyBuf, valueBuf);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		public int getRecordsConsumed() {
			return this.recordsConsumed;
		}

		public ArrayList<SpillRecord> getIndexCacheList() {
			return indexCacheList;
		}

	}

	private WriterThread<K, V> writerThread;

	private RingBuffer rb = new RingBuffer(32, 1024 * 1024);
	byte[] currentBlock = null;
	ByteBuffer currentBB = null;
	int currentBlockPos = 0;
	DataInputBuffer lengthDataInputBuf = new DataInputBuffer();
	byte[] lengthBuf = new byte[INTEGER_SIZE];

	// Count the number of spilled files
	private int currentRun = 0;
	// must be -1 so spillNewRecord detects new partition
	private int currentPartition = -1;

	/**
	 * Creates a buffered writer to write the output of the map phase.
	 * 
	 * @param mapOutputFile
	 *            Manipulate the working area for the transient store for maps
	 *            and reduces.
	 * @param isFinalOutput
	 *            controls which file should be create. If true, the file
	 *            created will be a "file.out"; if false, the file created will
	 *            be a "spillX.out", where X is the current run.
	 * @throws IOException
	 * */
	public AsyncRunOutputWriter(final MapOutputFile mapOutputFile,
			final FileSystem rfs, final JobConf job, final Class<K> keyClass,
			final Class<V> valClass, final CompressionCodec codec,
			final Counter spilledRecordsCounter) throws IOException {

		if (mapOutputFile == null)
			throw new NullPointerException("mapOutputFile is null.");
		if (rfs == null)
			throw new NullPointerException("rfs is null.");
		if (valClass == null)
			throw new NullPointerException("valClass is null.");
		if (keyClass == null)
			throw new NullPointerException("keyClass is null.");
		if (spilledRecordsCounter == null)
			throw new NullPointerException("spilledRecordsCounter is null.");

		writerThread = new WriterThread<K, V>(rb, job, rfs, keyClass, valClass,
				codec, mapOutputFile, spilledRecordsCounter);
		writerThread.start();

	}

	public void finish() throws IOException {
		try {
			LOG.info("Finishing async writer...");
			LOG.info("Waiting for async writer");
			releaseBlock();
			rb.setFinished();

			writerThread.join();
			LOG.info("Records consumed: " + writerThread.getRecordsConsumed());
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

	public void write(DataInputBuffer key, DataInputBuffer value, int run,
			int partition) {
		try {
			spillRecord(key, value, run, partition);
		} catch (IOException e) {
			LOG.error("Log error..." + e.getMessage());
		}
	}

	private void newBlock(int run, int part) throws InterruptedException {
		if (currentBlock != null) {
			releaseBlock();
		}
		currentBlock = rb.produceRequest();
		currentBB = ByteBuffer.wrap(currentBlock);
		currentBB.putInt(run);
		currentBB.putInt(part);
		currentBlockPos = 2 * INTEGER_SIZE;
	}

	private void releaseBlock() {
		// zero-out rest of block
		for (int p = currentBlockPos; p < currentBlock.length; p++) {
			currentBlock[p] = 0;
		}
		rb.produceRelease();
	}

	private void copyToRingBuffer(DataInputBuffer key, DataInputBuffer value)
			throws InterruptedException, IOException {
		if (currentBlock == null) {
			newBlock(currentRun, currentPartition);
		}

		int valueLen = value.getLength() - value.getPosition();
		int keyLen = (key.getLength() - key.getPosition());
		int size = keyLen + valueLen + INTEGER_SIZE * 2;

		int available = currentBlock.length - currentBlockPos;

		if (available < size) {
			newBlock(currentRun, currentPartition);
			available = currentBlock.length;

			if (available < size) {
				throw new IOException("Record too large " + size);
			}
		}

		ByteBuffer.wrap(currentBlock).putInt(currentBlockPos, keyLen);
		System.arraycopy(key.getData(), key.getPosition(), currentBlock,
				currentBlockPos + INTEGER_SIZE, keyLen);
		currentBlockPos += keyLen + INTEGER_SIZE;

		ByteBuffer.wrap(currentBlock).putInt(currentBlockPos, valueLen);
		System.arraycopy(value.getData(), value.getPosition(), currentBlock,
				currentBlockPos + INTEGER_SIZE, valueLen);

		currentBlockPos += valueLen + INTEGER_SIZE;
	}

	private void spillRecord(DataInputBuffer key, DataInputBuffer value,
			int run, int partition) throws IOException {

		System.out.println("Current  R/P: " + currentRun + "/"
				+ currentPartition + " Incoming R/P: " + run + "/" + partition);
		System.out.println();

		try {
			if (run != currentRun || partition != currentPartition) {
				newBlock(run, partition);
			}

			currentRun = run;
			currentPartition = partition;

			copyToRingBuffer(key, value);

		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

	/**
	 * Return the number of spills written.
	 * */
	public int getNumSpills() {
		return currentRun + 1;
	}

	public ArrayList<SpillRecord> getIndexCacheList() {

		return writerThread.getIndexCacheList();
	}

}
