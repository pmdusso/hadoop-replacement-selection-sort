package io;

public class RingBuffer {

	private byte[][] buffer;
	private Integer begin = 0;
	private Integer end = 0;
	private boolean bParity = true;
	private boolean eParity = true;
	private volatile boolean finished = false;

	public void setFinished() {
		this.finished = true;
	}

	public boolean isFinished() {
		return this.finished;
	}

	public RingBuffer(int blockCount, int blockSize) {
		buffer = new byte[blockCount][];
		for (int i = 0; i < blockCount; i++) {
			buffer[i] = new byte[blockSize];
		}
	}

	public boolean isFull() {
		return (begin.equals(end)) && (bParity != eParity);
	}

	public boolean isEmpty() {
		return (begin.equals(end)) && (bParity == eParity);
	}

	byte[] consumeRequest() throws InterruptedException {
		while (isEmpty()) {
			synchronized (begin) {
				begin.wait(1000);
				if (finished) {
					return null;
				}
			}
		}
		return buffer[begin];
	}

	public void consumeRelease() {
		boolean wasFull = isFull();
		incrBegin();
		if (wasFull) {
			synchronized (end) {
				end.notify();
			}
		}
	}

	private void incrBegin() {
		begin++;
		if (begin == buffer.length) {
			begin = 0;
			bParity = !bParity;
		}
	}

	byte[] produceRequest() throws InterruptedException {
		if (isFull()) {
			synchronized (end) {
				end.wait();
			}
		}
		return buffer[end];
	}

	public void produceRelease() {
		boolean wasEmpty = isEmpty();
		incrEnd();
		if (wasEmpty) {
			synchronized (begin) {
				begin.notify();
			}
		}
	}

	private void incrEnd() {
		end++;
		if (end == buffer.length) {
			end = 0;
			eParity = !eParity;
		}
	}

}
