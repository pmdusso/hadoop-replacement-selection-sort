package io;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalDirAllocator;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.mapred.Counters.Counter;
import org.apache.hadoop.mapred.IFile.Writer;
import org.apache.hadoop.mapred.IndexRecord;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapOutputFile;
import org.apache.hadoop.mapred.SpillRecord;

import replacementselection.HeapEntry;

public class RunOutputWriterWithOriginalWriter<K, V> {

	private static final Log LOG = LogFactory
			.getLog(RunOutputWriterWithOriginalWriter.class.getName());

	private Writer<K, V> writer;

	// The output stream for the output file

	FileSystem rfs;

	// Count the number of spilled files
	private int numSpills = 0;
	// Count the number of written records
	private long recordCounter = 0;

	private FSDataOutputStream outStream;

	private final MapOutputFile mapOutputFile;
	private int outputBufferSize;
	private final JobConf job;
	private final Class<K> keyClass;
	private final Class<V> valClass;
	private final CompressionCodec codec;
	private final Counter spilledRecordsCounter;

	private int currentPartition = -1;
	private long segmentStart = -1;
	private SpillRecord spillRec;

	private ArrayList<SpillRecord> indexCacheList;
	private int totalIndexCacheMemory;
	private static final int INDEX_CACHE_MEMORY_LIMIT = 1024 * 1024;

	/**
	 * Creates a buffered writer to write the output of the map phase.
	 * 
	 * @param mapOutputFile
	 *            Manipulate the working area for the transient store for maps
	 *            and reduces.
	 * @param isFinalOutput
	 *            controls which file should be create. If true, the file
	 *            created will be a "file.out"; if false, the file created will
	 *            be a "spillX.out", where X is the current run.
	 * */
	public RunOutputWriterWithOriginalWriter(final MapOutputFile mapOutputFile,
			final Boolean isFinalOutput, final FileSystem rfs,
			final JobConf job, final Class<K> keyClass,
			final Class<V> valClass, final CompressionCodec codec,
			final Counter spilledRecordsCounter) {

		if (mapOutputFile == null)
			throw new NullPointerException("mapOutputFile is null.");
		if (rfs == null)
			throw new NullPointerException("rfs is null.");
		if (valClass == null)
			throw new NullPointerException("valClass is null.");
		if (keyClass == null)
			throw new NullPointerException("keyClass is null.");
		if (spilledRecordsCounter == null)
			throw new NullPointerException("spilledRecordsCounter is null.");

		this.mapOutputFile = mapOutputFile;
		this.job = job;
		this.keyClass = keyClass;
		this.valClass = valClass;
		this.codec = codec;
		this.spilledRecordsCounter = spilledRecordsCounter;
		this.rfs = rfs;
		this.outputBufferSize = 4096;
		indexCacheList = new ArrayList<SpillRecord>();
		start(isFinalOutput);
	}

	public void write(DataInputBuffer key, DataInputBuffer value, int partition) {
		try {
			spillRecord(key, value, partition);
		} catch (IOException e) {
			LOG.error("Log error..." + e.getMessage());
		}
	}

	private void spillRecord(DataInputBuffer key, DataInputBuffer value,
			int partition) throws IOException {

		if (partition != currentPartition && currentPartition >= 0) {
			// Here, the previously sequence of records from a partition have
			// ended.
			// We have to create a IndexRecord object and save on it where does
			// the segment corresponding to that partition is the spill file

			writer.close();
			addIndexRecordToSpillRecords();
			writer = new Writer<K, V>(job, outStream, keyClass, valClass,
					codec, spilledRecordsCounter);

			// Prepare for the next partition
			// TODO: check if the getPos() method really return the position of
			// the stream inside the writer
			segmentStart = outStream.getPos();
			// segmentStart = writer.getRawoutBufferPosition();
			currentPartition = partition;
		}

		if (currentPartition == -1) {
			// TODO: check if the getPos() method really return the position of
			// the stream inside the writer
			segmentStart = outStream.getPos();
			// segmentStart = writer.getRawoutBufferPosition();
			currentPartition = partition;
		}

		final long recordStart = outStream.getPos();
		writer.append(key, value);
		recordCounter++;
		spilledRecordsCounter.increment(outStream.getPos() - recordStart);
	}

	public void write(final HeapEntry<K, V> h) {
		try {
			spillRecord(h.getKey(), h.getValue(), h.getPartition());
		} catch (IOException e) {
			LOG.error("Log error..." + e.getMessage());
		}
	}

	public void write(K key, V value, int partition) {
		try {
			spillRecord(key, value, partition);
		} catch (IOException e) {
			LOG.error("Log error..." + e.getMessage());
		}
	}

	private void spillRecord(final K key, final V value, int partition)
			throws IOException {

		if (partition != currentPartition && currentPartition >= 0) {
			// Here, the previously sequence of records from a partition have
			// ended.
			// We have to create a IndexRecord object and save on it where does
			// the segment corresponding to that partition is the spill file

			writer.close();
			addIndexRecordToSpillRecords();
			writer = new Writer<K, V>(job, outStream, keyClass, valClass,
					codec, spilledRecordsCounter);

			// Prepare for the next partition
			// TODO: check if the getPos() method really return the position of
			// the stream inside the writer
			segmentStart = outStream.getPos();
			// segmentStart = writer.getRawoutBufferPosition();
			currentPartition = partition;
		}

		if (currentPartition == -1) {
			// TODO: check if the getPos() method really return the position of
			// the stream inside the writer
			segmentStart = outStream.getPos();
			// segmentStart = writer.getRawoutBufferPosition();
			currentPartition = partition;
		}

		final long recordStart = outStream.getPos();
		writer.append(key, value);
		recordCounter++;
		spilledRecordsCounter.increment(outStream.getPos() - recordStart);
	}

	private void addIndexRecordToSpillRecords() throws IOException {
		IndexRecord rec = new IndexRecord();

		rec.startOffset = segmentStart;
		rec.rawLength = writer.getRawLength();
		rec.partLength = writer.getCompressedLength();

		spillRec.putIndex(rec, currentPartition);
	}

	/**
	 * Close the current spill and create a new one.
	 * */
	public void reset() {
		LOG.info(String
				.format("Run number %d closed with %d records written. Starting new run file.",
						numSpills, recordCounter));
		recordCounter = 0;
		flush();
		start(false);
	}

	/**
	 * Create a new spill file. This can be a final output file, a file which is
	 * the merged result of all spill files, or a simple spill.
	 * */
	private void start(final Boolean isFinalOutput) {

		if (outputBufferSize < 4096) {
			outputBufferSize = 4096;
		}

		try {
			outStream = null;
			Path p = null;
			spillRec = null;

			if (isFinalOutput) {
				p = mapOutputFile
						.getOutputFileForWrite(LocalDirAllocator.SIZE_UNKNOWN);
				outStream = rfs.create(p, true, outputBufferSize);
				writer = new Writer<K, V>(job, outStream, keyClass, valClass,
						codec, spilledRecordsCounter);
				spillRec = new SpillRecord(job.getNumReduceTasks());
				LOG.info("Created a final output file for write.");
			} else {
				p = mapOutputFile.getSpillFileForWrite(numSpills++,
						LocalDirAllocator.SIZE_UNKNOWN);
				outStream = rfs.create(p, true, outputBufferSize);
				writer = new Writer<K, V>(job, outStream, keyClass, valClass,
						codec, spilledRecordsCounter);
				spillRec = new SpillRecord(job.getNumReduceTasks());
				LOG.info("Created a spill output file for write.");
			}

		} catch (final IOException e) {
			LOG.error("Log error..." + e.getMessage());
		}
	}

	public void flush() {
		try {

			if (writer != null) {
				if (recordCounter > 0) {
					LOG.info(String.format(
							"Run number %d closed with %d records written.",
							numSpills, recordCounter));
				}
				writer.close();

				// This must be called when ALL PARTITIONS have been written.
				// The index cache list has one SpillRec object for each spilled
				// file.
				addIndexRecordToSpillRecords();

				// fix the index because we count spills from zero
				if (currentPartition < job.getNumReduceTasks() - 1) {
					for (int i = currentPartition + 1; i < job
							.getNumReduceTasks(); i++) {
						// TODO: check if the getPos() method really return the
						// position of the stream inside the writer
						segmentStart = outStream.getPos();
						// segmentStart = writer.getRawoutBufferPosition();
						currentPartition = i;
						writer = new Writer<K, V>(job, outStream, keyClass,
								valClass, codec, spilledRecordsCounter);
						writer.close();
						addIndexRecordToSpillRecords();
					}
				}
				addToCacheList();
			}
		} catch (final IOException e) {
			LOG.error("Log error..." + e.getMessage());

		} finally {
			writer = null;
			currentPartition = -1;

			try {
				if (outStream != null) {
					outStream.close();
				}
			} catch (final IOException e) {
			}

			outStream = null;
		}
	}

	private void addToCacheList() throws IOException {
		if (totalIndexCacheMemory >= INDEX_CACHE_MEMORY_LIMIT) {
			// create spill index file
			Path indexFilename = mapOutputFile.getSpillIndexFileForWrite(
					numSpills, job.getNumReduceTasks() * 24);
			spillRec.writeToFile(indexFilename, job);
		} else {
			indexCacheList.add(spillRec);
			totalIndexCacheMemory += spillRec.size() * 24;
		}
	}

	/**
	 * Return the number of records written.
	 * */
	public long getRecordCounter() {
		return recordCounter;
	}

	/**
	 * Return the number of spills written.
	 * */
	public int getNumSpills() {
		return numSpills;
	}

	public SpillRecord getSpillRecord() {
		return spillRec;
	}

	public ArrayList<SpillRecord> getIndexCacheList() {

		return indexCacheList;
	}

}
