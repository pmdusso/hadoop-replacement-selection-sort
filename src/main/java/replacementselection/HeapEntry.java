package replacementselection;

/**
 * This class have four fields: a key, a value, a partition and active status.
 * The first two (<code>key/value</code>) are the intermediate result of the Map
 * function. The third, <code>partition</code>, is the calculated partition
 * based on the key/value where the record should be send after the Map phase.
 * Each partition correspond to one Reduce task. The fourth, <code>active</code>
 * , is the current status of the heap entry with respect to replacement
 * selection algorithm.
 */
public class HeapEntry<K, V> {
	private K key;
	private V value;
	private int partition;
	private boolean active;
	private boolean currentActiveStatus;

	@Override
	public String toString() {
		return "[key=" + key + ", value=" + value + ", partition=" + partition
				+ "]";
	}

	public Boolean getActive() {
		return active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + partition;
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		HeapEntry other = (HeapEntry) obj;
		if (key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!key.equals(other.key)) {
			return false;
		}
		if (partition != other.partition) {
			return false;
		}
		return true;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public int getPartition() {
		return partition;
	}

	public boolean getCurrentActiveStatus() {
		return currentActiveStatus;
	}

	/**
	 * Constructs a HeapEntry object.
	 * 
	 * @param key
	 *            The key of the record
	 * @param value
	 *            The value of the record
	 * @param partition
	 *            Which partition this record will be assigned to
	 * */
	public HeapEntry(K key, V value, int partition, Boolean currentActiveStatus) {
		super();
		this.key = key;
		this.value = value;
		this.partition = partition;
		// When the record entry is first created, it should be active by
		// default;
		this.active = currentActiveStatus;
	}

}