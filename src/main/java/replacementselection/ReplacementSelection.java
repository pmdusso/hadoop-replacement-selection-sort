package replacementselection;

import io.RunOutputWriterWithOriginalWriter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.mapred.Counters.Counter;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapOutputFile;
import org.apache.hadoop.mapred.SpillRecord;

public class ReplacementSelection<K, V> {

	private static final Log LOG = LogFactory.getLog(ReplacementSelection.class
			.getName());

	private PriorityQueue<HeapEntry<K, V>> values;
	private Boolean currentActiveStatus;
	private HeapEntry<K, V> lastSmallest;
	private final HeapEntryComparator<K, V> heapEntryComparator;
	private final int memSize;

	private final RunOutputWriterWithOriginalWriter<K, V> originalOutputWriter;

	private ArrayList<SpillRecord> indexCacheList;

	public ArrayList<SpillRecord> getIndexCacheList() {
		return indexCacheList;
	}

	/**
	 * Create a ReplacementSelection object, writing the spill runs using hadoop
	 * IFile.Writer class.
	 * 
	 * @param memSize
	 *            The total size of the buffer, in records
	 * @param keyComparator
	 *            A custom comparator for the key of the records
	 * @param mapOutputFile
	 *            Hadoop object to handle output files and spills
	 * @param rfs
	 *            File system object
	 * @param job
	 *            A job conf object
	 * @param keyClass
	 *            The class (type) of the key
	 * @param valClass
	 *            The class (type) of the value
	 * @param codec
	 *            A compression codec
	 * @param spilledRecordsCounter
	 *            A Hadoop counter of the spilled records
	 * */
	public ReplacementSelection(final int memSize,
			final RawComparator<K> keyComparator,
			final MapOutputFile mapOutputFile, final FileSystem rfs,
			final JobConf job, final Class<K> keyClass,
			final Class<V> valClass, final CompressionCodec codec,
			final Counter spilledRecordsCounter) {
		this.memSize = memSize;
		this.currentActiveStatus = true;
		this.heapEntryComparator = new HeapEntryComparator<K, V>(keyComparator);

		this.values = new PriorityQueue<HeapEntry<K, V>>(memSize,
				heapEntryComparator);

		this.originalOutputWriter = new RunOutputWriterWithOriginalWriter<K, V>(
				mapOutputFile, false, rfs, job, keyClass, valClass, codec,
				spilledRecordsCounter);

		LOG.info("Replacement Selection object created successfully");

	}

	/**
	 * Add new Heap entries to the heap. While the buffer is not full, just add
	 * entries. When it is full, start processing
	 * */
	public void add(final K key, final V value, final int partition)
			throws Exception {
		if (values.size() < memSize) {
			HeapEntry<K, V> heapEntry = new HeapEntry<K, V>(key, value,
					partition, currentActiveStatus);
			values.add(heapEntry);
		} else {
			processEntry(key, value, partition);
		}
	}

	/**
	 * Flush the remaining entries in the heap, after the add() method stop
	 * being called.
	 * */
	public int flush() {
		return flushInternal();
	}

	private int flushInternal() {
		if (originalOutputWriter != null) {
			processHeap();
			this.values = null;
			indexCacheList = originalOutputWriter.getIndexCacheList();
			originalOutputWriter.flush();

			return originalOutputWriter.getNumSpills();
		}
		return -1;

	}

	private void resetInternal() {
		if (originalOutputWriter != null) {
			originalOutputWriter.reset();
		}
	}

	private void writeInternal(final HeapEntry<K, V> h) {
		if (originalOutputWriter != null) {
			originalOutputWriter.write(h);
		}
	}

	private void revertStatus() {
		if (values.size() > 0
				&& values.peek().getActive() != currentActiveStatus) {
			// If the smallest value is inactive, that means that there
			// are no active values anymore. We should mark all the
			// entries are active and initiate a new run.

			resetInternal();

			// If all slots are inactive, mark them all active.
			currentActiveStatus = !currentActiveStatus;
			lastSmallest = null;
		}
	}

	private void processEntry(final K key, final V value, final int partition) {
		// Find the smallest value that is still active until no values
		// are left to process:
		while (values.peek() != null
				&& values.peek().getActive() == currentActiveStatus) {

			if (lastSmallest == null) {
				lastSmallest = values.peek();
			}

			if (heapEntryComparator.compare(values.peek(), lastSmallest) >= 0) {
				// The currentSmallest is bigger then the lastSmallest.
				// For example, if in the previously iteration we output
				// a <0;44> and the current smallest is <1;77>, proceed

				lastSmallest = values.poll();
				writeInternal(lastSmallest);

				// Read from the input device into the slot where the
				// old element was.
				values.add(new HeapEntry<K, V>(key, value, partition,
						currentActiveStatus));

				break;
			} else {
				// In this case, the current smallest value is smaller
				// than the last smallest. This can happen when, after
				// we have written the last smallest value to the
				// output, the new value read from the input is smaller.
				// In this case, we must set it as inactive (active =
				// false), and the value will have to wait for a new run

				// If it was smaller than the old element, mark the old
				// slot inactive.
				final HeapEntry<K, V> tempHeapEntry = values.poll();
				tempHeapEntry.setActive(!currentActiveStatus);
				values.add(tempHeapEntry);
			}

			revertStatus();
		}
	}

	private void processHeap() {
		// Find the smallest value that is still active until no values
		// are left to process:
		while (!values.isEmpty()) {

			if (values.peek().getActive() == currentActiveStatus) {

				if (lastSmallest == null) {
					lastSmallest = values.peek();
				}

				if (heapEntryComparator.compare(values.peek(), lastSmallest) >= 0) {
					// The currentSmallest is bigger then the lastSmallest.
					// For example, if in the previously iteration we output
					// a <0;44> and the current smallest is <1;77>, proceed

					lastSmallest = values.poll();
					writeInternal(lastSmallest);

				} else {
					// In this case, the current smallest value is smaller
					// than the last smallest. This can happen when, after
					// we have written the last smallest value to the
					// output, the new value read from the input is smaller.
					// In this case, we must set it as inactive (active =
					// false), and the value will have to wait for a new run

					// If it was smaller than the old element, mark the old
					// slot inactive.
					final HeapEntry<K, V> tempHeapEntry = values.poll();
					tempHeapEntry.setActive(!currentActiveStatus);
					values.add(tempHeapEntry);
				}
			} else {
				revertStatus();
			}
		}
	}

	/**
	 * HeapEntryComparator is a custom comparator object used to specify
	 * ordering to a collection of HeapEntry objects. It extends the Comparator
	 * interface, overriding the compare() method. When constructed, the class
	 * receives as argument a custom Comparator object to compare the key
	 * fields.
	 * 
	 * The ordering is given by: first the active status (true < false); second,
	 * the partition field of the HeapEntry object; third, the ordering defined
	 * by the received keyComparator when the object is constructed.
	 * */
	@SuppressWarnings("hiding")
	private class HeapEntryComparator<K, V> implements
			Comparator<HeapEntry<K, V>> {

		private final Comparator<K> keyComparator;

		/**
		 * Constructs a HeapEntryComparator object.
		 * 
		 * @param keyComparator
		 *            A custom Comparator used to impose ordering to the generic
		 *            typed keys.
		 * */
		public HeapEntryComparator(final Comparator<K> keyComparator) {
			this.keyComparator = keyComparator;
		}

		@Override
		public int compare(final HeapEntry<K, V> o1, final HeapEntry<K, V> o2) {
			int r = 0;
			if (currentActiveStatus == true) {
				r = -o1.getActive().compareTo(o2.getActive());
			} else {
				r = o1.getActive().compareTo(o2.getActive());
			}

			if (r == 0) {
				if (o1.getPartition() != o2.getPartition()) {
					r = o1.getPartition() - o2.getPartition();
				} else {
					r = keyComparator.compare(o1.getKey(), o2.getKey());
				}
			}

			return r;
		}
	}
}
