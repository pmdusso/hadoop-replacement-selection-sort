package replacementselection;

import java.util.concurrent.TimeUnit;

public class Timer {
	private static boolean start = true;
	private static long startTime = 0;

	public static void checkIn() {
		if (start) {
			startTime = System.nanoTime();
		}
		start = false;
	}

	public static void checkOut(String message) {
		long totalTime = System.nanoTime() - startTime;
		System.out.println(message
				+ TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS));
		start = true;
	}
}
