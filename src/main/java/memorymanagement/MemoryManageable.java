package memorymanagement;

import java.util.AbstractMap.SimpleEntry;

public interface MemoryManageable {

	public abstract int getNumberOfExtents();

	public abstract int getAvailableFreeMemory();

	public abstract int getAvailableContiguousFreeMemory();

	/**
	 * Return a global memory address within the buffer, guaranteed to have at
	 * least the record's size space available.
	 * */
	public abstract int grabMemoryAddress(int recordSize);

	public abstract Block grabMemoryBlock(int recordSize);

	/**
	 * When a record is outputted from the buffer to the disk, the space it used
	 * to occupy becomes available, and should be returned to the free memory
	 * blocks list.
	 * 
	 * @param recordLength
	 * */
	public abstract void restoreMemory(int globalAddress, int recordLength);

	public abstract void restoreMemoryBlock(Block b);

	/**
	 * Buffer are divided in many extents. In order to obtain a global address
	 * for the memory block we obtained from the extent, we multiple the index
	 * of the extent by the size of each extent and add to the current address
	 * within the extent.
	 * 
	 * Example: given each extent is 8K and given we returned the address 256
	 * inside the third (index == 2) extent. We add 2 * 8K (from the first two
	 * extents) to the current 256 address to obtain 16640.
	 * */
	public abstract int translateFromExtentToGlobalAddress(int address, int i);

	/**
	 * This method executes the translation to the other side. Please check @translateFromExtentToGlobalAddress
	 * */
	public abstract SimpleEntry<Integer, Integer> translateFromGlobalAddressToExtent(
			int globalAddress);

	public abstract void defrag();

}