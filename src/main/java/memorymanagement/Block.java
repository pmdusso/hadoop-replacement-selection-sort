package memorymanagement;

public class Block {

	@Override
	public String toString() {
		return "[address=" + address + ", roundedRecordLength="
				+ roundedRecordLength + "]";
	}

	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;
		result = prime * result + address;
		result = prime * result + listFoundIndex;
		result = prime * result + roundedRecordLength;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Block other = (Block) obj;
		if (address != other.address)
			return false;
		if (listFoundIndex != other.listFoundIndex)
			return false;
		if (roundedRecordLength != other.roundedRecordLength)
			return false;
		return true;
	}

	public final int address;
	public final int listFoundIndex;
	public final int roundedRecordLength;

	public Block(final int address, final int listFoundIndex,
			final int roundedRecordLength) {
		this.address = address;
		this.listFoundIndex = listFoundIndex;
		this.roundedRecordLength = roundedRecordLength;
	}
}
