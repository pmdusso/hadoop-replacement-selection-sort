package memorymanagement;

import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedList;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.math3.exception.NullArgumentException;

public class ManageWithLinkedList implements MemoryManageable {

	private LinkedList<Integer>[] lists;
	private final int minBlockSize;
	private final int numberOfLists;

	@SuppressWarnings("unchecked")
	public ManageWithLinkedList(int numberOfExtents, final int extentSize,
			int increment) {

		if (numberOfExtents == 0 || extentSize == 0 || increment == 0)
			throw new NullArgumentException();

		this.minBlockSize = increment;
		numberOfLists = extentSize / increment;

		lists = new LinkedList[numberOfLists];

		lists[numberOfLists - 1] = new LinkedList<Integer>();

		for (int i = 0; i < numberOfExtents; i++) {
			lists[numberOfLists - 1].add(i * extentSize);
		}

	}

	@Override
	public int getNumberOfExtents() {
		return lists.length;
	}

	@Override
	public int getAvailableFreeMemory() {
		int total = 0;
		for (int i = 0; i < lists.length; i++) {
			if (lists[i] != null) {
				total += ((i + 1) * minBlockSize) * lists[i].size();
			}
		}
		return total;
	}

	@Override
	public int getAvailableContiguousFreeMemory() {
		for (int i = lists.length - 1; i >= 0; i--) {
			if (lists[i] != null && lists[i].size() > 0)
				return (i + 1) * minBlockSize;
		}
		return 0;
	}

	@Override
	public Block grabMemoryBlock(int recordSize) {
		final int roundedSize = roundUp(recordSize);
		int address = -1;
		int listFoundIndex = -1;

		for (int i = getListFoundIndex(roundedSize); i < numberOfLists; i++) {
			if (lists[i] != null && lists[i].size() > 0) {
				address = lists[i].poll();
				listFoundIndex = i;
				break;
			}
		}
		if (address == -1 && listFoundIndex == -1) {
			// No space left
			return null;
		}

		restoreExceedMemory(roundedSize, address, (listFoundIndex + 1)
				* minBlockSize);

		return new Block(address, getListFoundIndex(roundedSize), roundedSize);
	}

	public long grabMemoryLong(int recordSize) {
		final int roundedSize = roundUp(recordSize);
		int address = -1;
		int listFoundIndex = -1;

		for (int i = getListFoundIndex(roundedSize); i < numberOfLists; i++) {
			if (lists[i] != null && lists[i].size() > 0) {
				address = lists[i].poll();
				listFoundIndex = i;
				break;
			}
		}
		if (address == -1 && listFoundIndex == -1) {
			// No space left
			return -1;
		}

		restoreExceedMemory(roundedSize, address, (listFoundIndex + 1)
				* minBlockSize);

		return (long) address << 32 | roundedSize & 0xFFFFFFFFL;
	}

	/**
	 * If the block memory in the list is bigger than the record, we return the
	 * remaining memory to the memory list
	 * */
	private void restoreExceedMemory(final int roundedSize, int address,
			final int blockListSize) {
		if (blockListSize > roundedSize) {

			final int remainingListIndex = ((blockListSize - roundedSize) / minBlockSize) - 1;

			if (lists[remainingListIndex] == null) {
				lists[remainingListIndex] = new LinkedList<Integer>();
			}

			lists[remainingListIndex].add(address + roundedSize);
		}
	}

	/**
	 * Get the index in the list of blocks of where a block of given size should
	 * be placed. For example, if the minBlockSize == 32, we will have lists for
	 * 32, 64, 96, 128... Thus, the 128 block should be located at (128 / 32) -1
	 * = 3
	 * */
	private int getListFoundIndex(final int roundedSize) {
		return (roundedSize / minBlockSize) - 1;
	}

	@Override
	public void restoreMemoryBlock(Block blockToRestore) {

		try {
			if (lists[blockToRestore.listFoundIndex] == null) {
				lists[blockToRestore.listFoundIndex] = new LinkedList<Integer>();
			}

			lists[blockToRestore.listFoundIndex].add(blockToRestore.address);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Restore a block memory, represent by a long value which contains two
	 * ints: the first is the address of the block, and the second is the
	 * rounded size of it.
	 * */
	public void restoreMemoryBlock(long blockToRestore) {

		int listFoundIndex = getListFoundIndex((int) blockToRestore);

		if (lists[listFoundIndex] == null) {
			lists[listFoundIndex] = new LinkedList<Integer>();
		}

		lists[listFoundIndex].add((int) (blockToRestore >> 32));

	}

	/**
	 * Restore a block memory, represent by a INT value the rounded size of the
	 * block.
	 * */
	public void restoreMemoryBlock(int address, int length) {
		int listFoundIndex = getListFoundIndex(length);

		if (lists[listFoundIndex] == null) {
			lists[listFoundIndex] = new LinkedList<Integer>();
		}

		lists[listFoundIndex].add(address);

	}

	@Override
	public void restoreMemory(int globalAddress, int recordLength) {
		throw new NotImplementedException();
	}

	public int roundUp(int recordSize) {
		return (int) (Math.ceil(recordSize / (double) minBlockSize) * (double) minBlockSize);
	}

	@Override
	public int translateFromExtentToGlobalAddress(int address, int index) {
		throw new NotImplementedException();
	}

	@Override
	public SimpleEntry<Integer, Integer> translateFromGlobalAddressToExtent(
			final int globalAddress) {
		throw new NotImplementedException();
	}

	@Override
	public void defrag() {
		throw new NotImplementedException();
	}

	@Override
	public int grabMemoryAddress(int recordSize) {
		throw new NotImplementedException();

	}

}
