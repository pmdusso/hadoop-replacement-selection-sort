package pluggable;

import io.AsyncRunOutputWriter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

import memorymanagement.Block;
import memorymanagement.ManageWithLinkedList;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.io.serializer.SerializationFactory;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapred.IFile.Writer;
import org.apache.hadoop.mapred.IndexRecord;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapOutputCollector;
import org.apache.hadoop.mapred.MapOutputFile;
import org.apache.hadoop.mapred.MapTask;
import org.apache.hadoop.mapred.Merger;
import org.apache.hadoop.mapred.Merger.Segment;
import org.apache.hadoop.mapred.RawKeyValueIterator;
import org.apache.hadoop.mapred.SpillRecord;
import org.apache.hadoop.mapred.Task.CombineOutputCollector;
import org.apache.hadoop.mapred.Task.CombinerRunner;
import org.apache.hadoop.mapred.Task.TaskReporter;
import org.apache.hadoop.mapred.TaskAttemptID;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.TaskCounter;
import org.apache.hadoop.mapreduce.TaskType;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.Progress;
import org.apache.hadoop.util.ReflectionUtils;

import replacementselection.Timer;

public class MapOutputHeap<K extends Object, V extends Object> implements MapOutputCollector<K, V>, IndexedSortable {

    private static final Log LOG = LogFactory.getLog(MapOutputHeap.class
            .getName());

    private int partitions;
    private JobConf job;
    private TaskReporter reporter;
    private Class<K> keyClass;
    private Class<V> valClass;
    private RawComparator<K> comparator;
    private SerializationFactory serializationFactory;
    private Serializer<K> keySerializer;
    private Serializer<V> valSerializer;
    final BlockingBuffer bb = new BlockingBuffer();

    private CombinerRunner<K, V> combinerRunner;
    private CombineOutputCollector<K, V> combineCollector;

    // Compression for map-outputs
    private CompressionCodec codec = null;

    // Replacement Selection
    private AsyncRunOutputWriter<K, V> outputWriter;
    private LightHeapEntry lastSpilledRecord = null;
    private byte[] transitionBuffer;
    private byte[] lastSpilledRecordKey = new byte[256];

    private ManageWithLinkedList theManager;
    private PriorityQueue<LightHeapEntry> selectionTree;
    private int currentRun;
    public int writtenBytesSoFar;

    // Buffer used to output key and value
    DataInputBuffer keyBuffer;
    DataInputBuffer valueBuffer;

    private FileSystem rfs;
    private int numSpills = 0;

    private Counters.Counter mapOutputByteCounter;
    private Counters.Counter mapOutputRecordCounter;
    private Counters.Counter fileOutputByteCounter;

    private Counters.Counter memoryResetRsCounter;
    private Counters.Counter mapOutputBlockRecordByteCounter;

    private MapTask mapTask;
    private MapOutputFile mapOutputFile;
    private Progress sortPhase;
    private Counters.Counter spilledRecordsCounter;
    private ArrayList<SpillRecord> indexCacheList;

    private int minSpillsForCombine;

    byte[] kvbuffer; // main output buffer
    private int extentSize; // size of the extents which divide the manage
    // memory
    private int minBlockSize;

    private static final int NMETA = 4; // num meta ints
    private static final int METASIZE = NMETA * 4; // size in bytes

    private static final int APPROX_HEADER_LENGTH = 150;
    private static final int MAP_OUTPUT_INDEX_RECORD_LENGTH = 24;

    private int kvbufferSize;

    @Override
    public int compare(final int i, final int j) {
        throw new NotImplementedException();
    }

    @Override
    public void swap(final int i, final int j) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void init(
            final org.apache.hadoop.mapred.MapOutputCollector.Context context)
            throws IOException, ClassNotFoundException {
        job = context.getJobConf();
        reporter = context.getReporter();
        mapTask = context.getMapTask();
        mapOutputFile = mapTask.getMapOutputFile();
        sortPhase = mapTask.getSortPhase();
        spilledRecordsCounter = reporter
                .getCounter(TaskCounter.SPILLED_RECORDS);
        partitions = job.getNumReduceTasks();
        rfs = FileSystem.getLocal(job).getRaw();

        indexCacheList = new ArrayList<SpillRecord>();

        // sanity checks
        final float spillper = job.getFloat("io.sort.spill.percent", (float) 0.8);
        final float recper = job.getFloat("io.sort.record.percent", (float) 0.05);
        final int sortmb = job.getInt("io.sort.mb", 100);
        if (spillper > (float) 1.0 || spillper < (float) 0.0) {
            throw new IOException("Invalid \"io.sort.spill.percent\": " + spillper);
        }
        if (recper > (float) 1.0 || recper < (float) 0.01) {
            throw new IOException("Invalid \"io.sort.record.percent\": "
                    + recper);
        }
        if ((sortmb & 0x7FF) != sortmb) {
            throw new IOException("Invalid \"io.sort.mb\": " + sortmb);
        }

        LOG.info("io.sort.mb = " + sortmb);
        // buffers and accounting

        // k/v serialization
        comparator = job.getOutputKeyComparator();
        keyClass = (Class<K>) job.getMapOutputKeyClass();
        valClass = (Class<V>) job.getMapOutputValueClass();
        serializationFactory = new SerializationFactory(job);
        keySerializer = serializationFactory.getSerializer(keyClass);
        keySerializer.open(bb);
        valSerializer = serializationFactory.getSerializer(valClass);
        valSerializer.open(bb);

        // spill
        int maxMemUsage = sortmb << 20;
        maxMemUsage -= maxMemUsage % METASIZE;
        extentSize = 64;
        minBlockSize = 32;
        // kvbufferSize = maxMemUsage / extentSize * extentSize;
        kvbufferSize = 256;
        kvbuffer = new byte[kvbufferSize];
        LOG.info("MapOutputHeap buffer = " + kvbufferSize);
        outputWriter = new AsyncRunOutputWriter<K, V>(mapOutputFile, rfs, job,
                keyClass, valClass, codec, spilledRecordsCounter);

        // Replacement Selection
        transitionBuffer = new byte[1024];
        theManager = new ManageWithLinkedList(kvbufferSize / extentSize,
                extentSize, minBlockSize);
        selectionTree = new PriorityQueue<LightHeapEntry>();
        currentRun = 0;
        writtenBytesSoFar = 0;

        // Write buffers
        keyBuffer = new DataInputBuffer();
        valueBuffer = new DataInputBuffer();

        // counters
        mapOutputByteCounter = reporter
                .getCounter(TaskCounter.MAP_OUTPUT_BYTES);
        mapOutputRecordCounter = reporter
                .getCounter(TaskCounter.MAP_OUTPUT_RECORDS);
        fileOutputByteCounter = reporter
                .getCounter(TaskCounter.MAP_OUTPUT_MATERIALIZED_BYTES);
        memoryResetRsCounter = reporter
                .getCounter(ReplacementSelectionCounter.MEMORY_RESET);
        mapOutputBlockRecordByteCounter = reporter
                .getCounter(ReplacementSelectionCounter.MEMORY_BLOCK_RECORD_SIZE);

        // compression
        if (job.getCompressMapOutput()) {
            final Class<? extends CompressionCodec> codecClass = job
                    .getMapOutputCompressorClass(DefaultCodec.class);
            codec = ReflectionUtils.newInstance(codecClass, job);
        } else {
            codec = null;
        }

        // combiner
        minSpillsForCombine = job.getInt(MRJobConfig.MAP_COMBINE_MIN_SPILLS, 3);
        final Counters.Counter combineInputCounter = reporter
                .getCounter(TaskCounter.COMBINE_INPUT_RECORDS);
        combinerRunner = CombinerRunner.create(job, getTaskID(),
                combineInputCounter, reporter, null);
        if (combinerRunner != null) {
            final Counters.Counter combineOutputCounter = reporter
                    .getCounter(TaskCounter.COMBINE_OUTPUT_RECORDS);
            combineCollector = new CombineOutputCollector<K, V>(
                    combineOutputCounter, reporter, job);
        } else {
            combineCollector = null;
        }

    }

    @Override
    public void collect(final K key, final V value, final int partition)
            throws IOException, InterruptedException {
        reporter.progress();
        Timer.checkIn();
        if (key.getClass() != keyClass) {
            throw new IOException("Type mismatch in key from map: expected "
                    + keyClass.getName() + ", recieved "
                    + key.getClass().getName());
        }
        if (value.getClass() != valClass) {
            throw new IOException("Type mismatch in value from map: expected "
                    + valClass.getName() + ", recieved "
                    + value.getClass().getName());
        }

        if (partition < 0 || partition >= partitions) {
            throw new IOException("Illegal partition for " + key + " ("
                    + partition + ")");
        }

        // We execute the loop until we add the current key/value pair into the
        // buffer. It may be necessary to
        // remove/output/spill several records from the buffer until we can add
        // the current one.
        // We serialize first the key and value in a temporary small buffer

        writtenBytesSoFar = 0;
        keySerializer.serialize(key);
        final int keyLength = writtenBytesSoFar;

        valSerializer.serialize(value);
        final int recordLength = writtenBytesSoFar;

        while (true) {
            // sufficient buffer space?
            Block memoryBlock = theManager.grabMemoryBlock(recordLength);
            if (memoryBlock != null) {
                // here, we know that we have sufficient space to write
                System.arraycopy(transitionBuffer, 0, kvbuffer,
                        memoryBlock.address, memoryBlock.roundedRecordLength);
                addToSelectionTree(partition, keyLength, recordLength,
                        memoryBlock);
                break;
            }
            flushFromSelectionTree(false, true);
        }
    }

    /**
     * Input process that fill memory with new records and adds them to the
     * selection tree. The last spilled record can be null when no record has
     * been spilled yet. In this case, we add the incoming record directly to
     * the selection tree. If the last spilled record is bigger than the
     * incoming record, the incoming record cannot belong to the current run
     * anymore. Thus, we add it with a run number currentRun + 1
     */
    private void addToSelectionTree(final int partition, final int keyLength,
                                    final int recordLength, final Block memoryBlock) {
        final LightHeapEntry incomingRecord = new LightHeapEntry(partition,
                keyLength, recordLength, memoryBlock);

        if (lastSpilledRecord != null) {
            if (lastSpilledRecord.compareToWithoutRun(incomingRecord) > 0) {
                incomingRecord.setRun(currentRun + 1);
            } else {
                incomingRecord.setRun(currentRun);
            }
        } else {
            incomingRecord.setRun(currentRun);
        }

        selectionTree.add(incomingRecord);
        System.out.println("Incoming entry" + incomingRecord);
    }

    /**
     * Whenever it fails to find memory space for an incoming record, it resumes
     * the output process, which runs until it has created a free slot of
     * sufficient size.
     *
     * @param isFinalFlush : we keep control of the final flush because, that case, we do
     *                     not have to restore memory anymore - which saves us some
     *                     instructions.
     */
    private void flushFromSelectionTree(final boolean isFinalFlush,
                                        final boolean isBatchFlush) {

        if (isBatchFlush && selectionTree.size() > 1000) {
            for (int i = 0; i < 1000; i++) {
                flushToWriter();
            }
        } else {
            flushToWriter();
        }
    }

    /**
     * If the run of the smallest record is bigger than the value of the current
     * run, that means that there are no records belonging to this (current) run
     * anymore. We should close this run and start the next one.
     */
    private void flushToWriter() {
        lastSpilledRecord = selectionTree.poll();

        if (lastSpilledRecord == null) {
            memoryResetRsCounter.increment(1);
            theManager = new ManageWithLinkedList(kvbufferSize / extentSize,
                    extentSize, minBlockSize);
            return;
        }

        if (lastSpilledRecord.getRun() > currentRun) {
            currentRun = lastSpilledRecord.getRun();
        }

        retoreMemoryAndSaveKey(lastSpilledRecord);
        writeToSpill(lastSpilledRecord);

    }

    /**
     * Save the (serialized) key of the last spilled record to use in a raw
     * comparison
     */
    private void retoreMemoryAndSaveKey(LightHeapEntry recordToRestore) {
        theManager.restoreMemoryBlock(recordToRestore.memoryBlock);

        if (recordToRestore.keyLength > lastSpilledRecordKey.length)
            lastSpilledRecordKey = new byte[lastSpilledRecordKey.length * 2];
        System.arraycopy(kvbuffer, recordToRestore.memoryBlock.address,
                lastSpilledRecordKey, 0, recordToRestore.keyLength);
    }

    private void writeToSpill(LightHeapEntry recordToSpill) {

        keyBuffer.reset(kvbuffer, recordToSpill.memoryBlock.address,
                recordToSpill.keyLength);
        valueBuffer.reset(kvbuffer, recordToSpill.memoryBlock.address
                + recordToSpill.keyLength, recordToSpill.recordLength
                - recordToSpill.keyLength);

        mapOutputRecordCounter.increment(1);
        mapOutputByteCounter.increment(recordToSpill.recordLength);
        mapOutputBlockRecordByteCounter
                .increment(recordToSpill.memoryBlock.roundedRecordLength);

        System.out.println("Flushed entry" + recordToSpill);
        outputWriter.write(keyBuffer, valueBuffer, recordToSpill.getRun(),
                recordToSpill.partition);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void flush() throws IOException, InterruptedException,
            ClassNotFoundException {
        LOG.info("Starting flush of map output");

        LightHeapEntry[] selectionArray = new MapOutputHeap.LightHeapEntry[selectionTree
                .size()];

        selectionTree.toArray(selectionArray);
        Arrays.sort(selectionArray);

        for (int i = 0; i < selectionArray.length; i++) {

            if (selectionArray[i].getRun() > currentRun) {
                currentRun = selectionArray[i].getRun();
            }

            writeToSpill(selectionArray[i]);
        }

        outputWriter.finish();
        numSpills = outputWriter.getNumSpills();
        indexCacheList = outputWriter.getIndexCacheList();
        Timer.checkOut("Run Generation: ");

        for (int p = 0; p < job.getNumReduceTasks(); p++) {
            for (SpillRecord s : indexCacheList) {
                System.out.println("Partition=" + p + "("
                        + s.getIndex(p).startOffset + ","
                        + s.getIndex(p).rawLength + ", "
                        + s.getIndex(p).partLength + ")");

            }
            System.out.println();
        }

        selectionTree = null;
        selectionArray = null;
        kvbuffer = null;
        transitionBuffer = null;
        theManager = null;

        Timer.checkIn();
        mergeParts();
        Timer.checkOut("Merge: ");
        printCounters();

        LOG.info("MapOutputHeap wrote " + mapOutputRecordCounter.getCounter()
                + " records.");
        final Path outputPath = mapOutputFile.getOutputFile();
        fileOutputByteCounter.increment(rfs.getFileStatus(outputPath).getLen());

    }

    @Override
    public void close() throws IOException, InterruptedException {

    }

    private TaskAttemptID getTaskID() {
        return mapTask.getTaskID();
    }

    private void mergeParts() throws IOException, InterruptedException,
            ClassNotFoundException {
        // get the approximate size of the final output/index files
        long finalOutFileSize = 0;
        long finalIndexFileSize = 0;
        final Path[] filename = new Path[numSpills];
        final TaskAttemptID mapId = getTaskID();

        for (int i = 0; i < numSpills; i++) {
            filename[i] = mapOutputFile.getSpillFile(i);
            finalOutFileSize += rfs.getFileStatus(filename[i]).getLen();
        }

        if (numSpills == 1) { // the spill is the final output
            rfs.rename(filename[0], new Path(filename[0].getParent(),
                    "file.out"));
            if (indexCacheList.size() == 0) {
                rfs.rename(mapOutputFile.getSpillIndexFile(0), new Path(
                        filename[0].getParent(), "file.out.index"));
            } else {
                indexCacheList.get(0).writeToFile(
                        new Path(filename[0].getParent(), "file.out.index"),
                        job);
            }
            sortPhase.complete();
            return;
        }

        // read in paged indices
        for (int i = indexCacheList.size(); i < numSpills; ++i) {
            final Path indexFileName = mapOutputFile.getSpillIndexFile(i);
            indexCacheList.add(new SpillRecord(indexFileName, job));
        }

        // make correction in the length to include the sequence file header
        // lengths for each partition
        finalOutFileSize += partitions * APPROX_HEADER_LENGTH;
        finalIndexFileSize = partitions * MAP_OUTPUT_INDEX_RECORD_LENGTH;
        final Path finalOutputFile = mapOutputFile
                .getOutputFileForWrite(finalOutFileSize);
        final Path finalIndexFile = mapOutputFile
                .getOutputIndexFileForWrite(finalIndexFileSize);

        // The output stream for the final single output file
        final FSDataOutputStream finalOut = rfs.create(finalOutputFile, true,
                4096);

        if (numSpills == 0) {
            // create dummy files
            final IndexRecord rec = new IndexRecord();
            final SpillRecord sr = new SpillRecord(partitions);
            try {
                for (int i = 0; i < partitions; i++) {
                    final long segmentStart = finalOut.getPos();
                    final Writer<K, V> writer = new Writer<K, V>(job, finalOut,
                            keyClass, valClass, codec, null);
                    writer.close();
                    rec.startOffset = segmentStart;
                    rec.rawLength = writer.getRawLength();
                    rec.partLength = writer.getCompressedLength();
                    sr.putIndex(rec, i);
                }
                sr.writeToFile(finalIndexFile, job);
            } finally {
                finalOut.close();
            }
            sortPhase.complete();
            return;
        }
        {
            sortPhase.addPhases(partitions); // Divide sort phase into
            // sub-phases

            final IndexRecord rec = new IndexRecord();
            final SpillRecord spillRec = new SpillRecord(partitions);
            for (int parts = 0; parts < partitions; parts++) {
                // create the segments to be merged
                final List<Segment<K, V>> segmentList = new ArrayList<Segment<K, V>>(
                        numSpills);
                for (int i = 0; i < numSpills; i++) {
                    final IndexRecord indexRecord = indexCacheList.get(i)
                            .getIndex(parts);

                    final Segment<K, V> s = new Segment<K, V>(job, rfs,
                            filename[i], indexRecord.startOffset,
                            indexRecord.partLength, codec, true);
                    segmentList.add(i, s);

                    LOG.info("MapId=" + mapId + " Reducer=" + parts + "Spill ="
                            + i + "(" + indexRecord.startOffset + ","
                            + indexRecord.rawLength + ", "
                            + indexRecord.partLength + ")");

                }
                final int mergeFactor = job.getInt(MRJobConfig.IO_SORT_FACTOR,
                        100);
                // sort the segments only if there are intermediate merges
                final boolean sortSegments = segmentList.size() > mergeFactor;
                // merge
                @SuppressWarnings("unchecked")
                final RawKeyValueIterator kvIter = Merger.merge(job, rfs,
                        keyClass, valClass, codec, segmentList, mergeFactor,
                        new Path(mapId.toString()),
                        job.getOutputKeyComparator(), reporter, sortSegments,
                        null, spilledRecordsCounter, sortPhase.phase(),
                        TaskType.MAP);

                // write merged output to disk
                final long segmentStart = finalOut.getPos();
                final Writer<K, V> writer = new Writer<K, V>(job, finalOut,
                        keyClass, valClass, codec, spilledRecordsCounter);
                if (combinerRunner == null || numSpills < minSpillsForCombine) {
                    Merger.writeFile(kvIter, writer, reporter, job);
                } else {
                    combineCollector.setWriter(writer);
                    combinerRunner.combine(kvIter, combineCollector);
                }

                // close
                writer.close();

                sortPhase.startNextPhase();

                // record offsets
                rec.startOffset = segmentStart;
                rec.rawLength = writer.getRawLength();
                rec.partLength = writer.getCompressedLength();
                spillRec.putIndex(rec, parts);
            }
            spillRec.writeToFile(finalIndexFile, job);
            finalOut.close();

            // do not delete spills, we use them to count the number of runs
            // later
            /*
             * for (int i = 0; i < numSpills; i++) { rfs.delete(filename[i],
			 * true); }
			 */
        }

    }

    public void printCounters() {
        System.out.println(mapOutputRecordCounter.getDisplayName() + ": "
                + mapOutputRecordCounter.getValue());
        System.out.println(mapOutputByteCounter.getDisplayName() + ": "
                + mapOutputByteCounter.getValue());
        System.out.println(fileOutputByteCounter.getDisplayName() + ": "
                + fileOutputByteCounter.getValue());

        System.out.println(memoryResetRsCounter.getDisplayName() + ": "
                + memoryResetRsCounter.getValue());
        System.out.println(mapOutputBlockRecordByteCounter.getDisplayName()
                + ": " + mapOutputBlockRecordByteCounter.getValue());

    }

    /**********************************************************************************************
     * INNER CLASSES
     **********************************************************************************************/

	/*
	 * Inner class managing the spill of serialized records to disk.
	 */
    protected class BlockingBuffer extends DataOutputStream {

        public BlockingBuffer() {
            this(new Buffer());
        }

        private BlockingBuffer(final OutputStream out) {
            super(out);
        }

        /**
         * Mark end of record. Note that this is required if the buffer is to
         * cut the spill in the proper place.
         */
        public int markRecord() {
            return 0;
        }

        protected synchronized void reset() throws IOException {
            throw new NotImplementedException();
        }
    }

    public class Buffer extends OutputStream {

        private final byte[] scratch = new byte[1];

        @Override
        public synchronized void write(final int v) throws IOException {
            scratch[0] = (byte) v;
            write(scratch, 0, 1);
        }

        /**
         * Attempt to write a sequence of bytes to the transition buffer.
         */
        @Override
        public synchronized void write(final byte b[], final int off,
                                       final int len) throws IOException {

            if (len <= 1024) {
                System.arraycopy(b, off, transitionBuffer, writtenBytesSoFar,
                        len);
                writtenBytesSoFar += len;
            } else {
                throw new RuntimeException(
                        "Key or value does not fit in the transition buffer");
            }
        }
    }

    @SuppressWarnings({"rawtypes"})
    class LightHeapEntry implements Comparable<LightHeapEntry> {

        @Override
        public String toString() {
            return "LightHeapEntry [run=" + run + ", partition=" + partition
                    + ", memoryBlock=" + memoryBlock + "]";
        }

        private int run;
        public final int partition;
        public final int keyLength;
        public final int recordLength;
        public final Block memoryBlock;

        /**
         * Constructor
         *
         * @param partition
         * @param keyPrefix
         * @param keyLength
         * @param comparator
         * @param memoryBlock
         */
        public LightHeapEntry(final int partition, final int keyLength,
                              final int recordLength, final Block memoryBlock) {
            super();

            this.partition = partition;

            this.keyLength = keyLength;
            this.recordLength = recordLength;
            this.memoryBlock = memoryBlock;
        }

		/*
		 * public int compare(final byte[] b1, final int s1, final int l1, final
		 * byte[] b2, final int s2, final int l2) { return
		 * WritableComparator.compareBytes(b1, s1, l1, b2, s2, l2); }
		 */

        @Override
        public int compareTo(final LightHeapEntry that) {
            if (this == that) {
                return EQUAL;
            }
            if (this.run < that.run) {
                return BEFORE;
            }
            if (this.run > that.run) {
                return AFTER;
            }
            if (this.partition < that.partition) {
                return BEFORE;
            }
            if (this.partition > that.partition) {
                return AFTER;
            }
            int r = comparator.compare(kvbuffer, this.memoryBlock.address,
                    this.keyLength, kvbuffer, that.memoryBlock.address,
                    that.keyLength);

            return r;
        }

        private final static int BEFORE = -1;
        private final static int EQUAL = 0;
        private final static int AFTER = 1;

        public int compareToWithoutRun(final LightHeapEntry that) {

            if (this == that) {
                return EQUAL;
            }

            if (this.partition < that.partition) {
                return BEFORE;
            }
            if (this.partition > that.partition) {
                return AFTER;
            }

            final int comparison = comparator.compare(lastSpilledRecordKey, 0,
                    this.keyLength, kvbuffer, that.memoryBlock.address,
                    that.keyLength);

            if (comparison != EQUAL) {
                return comparison;
            }

            assert this.equals(that) : "compareTo inconsistent with equals.";

            return EQUAL;
        }

        public int getRun() {
            return run;
        }

        public void setRun(final int currentRun) {
            this.run = currentRun;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + keyLength;
            result = prime * result
                    + ((memoryBlock == null) ? 0 : memoryBlock.hashCode());
            result = prime * result + partition;
            result = prime * result + recordLength;
            result = prime * result + run;
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            LightHeapEntry other = (LightHeapEntry) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (keyLength != other.keyLength)
                return false;
            if (memoryBlock == null) {
                if (other.memoryBlock != null)
                    return false;
            } else if (!memoryBlock.equals(other.memoryBlock))
                return false;
            if (partition != other.partition)
                return false;
            if (recordLength != other.recordLength)
                return false;
            if (run != other.run)
                return false;
            return true;
        }

        private MapOutputHeap getOuterType() {
            return MapOutputHeap.this;
        }

    }

}
