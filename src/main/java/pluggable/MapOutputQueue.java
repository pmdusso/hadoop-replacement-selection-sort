package pluggable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapred.IFile.Writer;
import org.apache.hadoop.mapred.IndexRecord;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapOutputCollector;
import org.apache.hadoop.mapred.MapOutputFile;
import org.apache.hadoop.mapred.MapTask;
import org.apache.hadoop.mapred.Merger;
import org.apache.hadoop.mapred.Merger.Segment;
import org.apache.hadoop.mapred.RawKeyValueIterator;
import org.apache.hadoop.mapred.SpillRecord;
import org.apache.hadoop.mapred.Task.CombineOutputCollector;
import org.apache.hadoop.mapred.Task.CombinerRunner;
import org.apache.hadoop.mapred.Task.TaskReporter;
import org.apache.hadoop.mapred.TaskAttemptID;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.TaskCounter;
import org.apache.hadoop.mapreduce.TaskType;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.Progress;
import org.apache.hadoop.util.ReflectionUtils;

import replacementselection.ReplacementSelection;
import replacementselection.Timer;

public class MapOutputQueue<K extends Object, V extends Object> implements
		MapOutputCollector<K, V>, IndexedSortable {

	private static final Log LOG = LogFactory.getLog(MapOutputQueue.class
			.getName());

	private int partitions;
	private JobConf job;
	private TaskReporter reporter;
	private Class<K> keyClass;
	private Class<V> valClass;
	private RawComparator<K> comparator;

	private CombinerRunner<K, V> combinerRunner;
	private CombineOutputCollector<K, V> combineCollector;

	// Compression for map-outputs
	private CompressionCodec codec = null;

	// Replacement Selection
	private ReplacementSelection<K, V> knuthBuffer;

	private FileSystem rfs;
	private int numSpills = 0;

	private Counters.Counter mapOutputRecordCounter;
	private Counters.Counter fileOutputByteCounter;

	private MapTask mapTask;
	private MapOutputFile mapOutputFile;
	private Progress sortPhase;
	private Counters.Counter spilledRecordsCounter;
	private ArrayList<SpillRecord> indexCacheList;

	private int minSpillsForCombine;

	private static final int APPROX_HEADER_LENGTH = 150;
	private static final int MAP_OUTPUT_INDEX_RECORD_LENGTH = 24;

	@Override
	public int compare(final int i, final int j) {
		return 0;
	}

	@Override
	public void swap(final int i, final int j) {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void init(
			final org.apache.hadoop.mapred.MapOutputCollector.Context context)
			throws IOException, ClassNotFoundException {
		job = context.getJobConf();
		reporter = context.getReporter();
		mapTask = context.getMapTask();
		mapOutputFile = mapTask.getMapOutputFile();
		sortPhase = mapTask.getSortPhase();
		spilledRecordsCounter = reporter
				.getCounter(TaskCounter.SPILLED_RECORDS);
		partitions = job.getNumReduceTasks();
		rfs = FileSystem.getLocal(job).getRaw();

		indexCacheList = new ArrayList<SpillRecord>();

		// sanity checks
		final float spillper = job.getFloat("io.sort.spill.percent",
				(float) 0.8);
		final float recper = job.getFloat("io.sort.record.percent",
				(float) 0.05);
		final int sortmb = job.getInt("io.sort.mb", 100);
		if (spillper > (float) 1.0 || spillper < (float) 0.0) {
			throw new IOException("Invalid \"io.sort.spill.percent\": "
					+ spillper);
		}
		if (recper > (float) 1.0 || recper < (float) 0.01) {
			throw new IOException("Invalid \"io.sort.record.percent\": "
					+ recper);
		}
		if ((sortmb & 0x7FF) != sortmb) {
			throw new IOException("Invalid \"io.sort.mb\": " + sortmb);
		}
		LOG.info("io.sort.mb = " + sortmb);

		// k/v serialization
		comparator = job.getOutputKeyComparator();
		keyClass = (Class<K>) job.getMapOutputKeyClass();
		valClass = (Class<V>) job.getMapOutputValueClass();

		// Replacement Selection
		int recordCapacity = sortmb << 20;
		recordCapacity -= recordCapacity % 100;
		recordCapacity /= 100;
		LOG.info("Replacement selection buffer (in records) = "
				+ recordCapacity);

		knuthBuffer = new ReplacementSelection<K, V>(recordCapacity,
				comparator, mapOutputFile, rfs, job, keyClass, valClass, codec,
				spilledRecordsCounter);

		// counters
		mapOutputRecordCounter = reporter
				.getCounter(TaskCounter.MAP_OUTPUT_RECORDS);
		fileOutputByteCounter = reporter
				.getCounter(TaskCounter.MAP_OUTPUT_MATERIALIZED_BYTES);

		// compression
		if (job.getCompressMapOutput()) {
			final Class<? extends CompressionCodec> codecClass = job
					.getMapOutputCompressorClass(DefaultCodec.class);
			codec = ReflectionUtils.newInstance(codecClass, job);
		} else {
			codec = null;
		}

		// combiner
		minSpillsForCombine = job.getInt(MRJobConfig.MAP_COMBINE_MIN_SPILLS, 3);
		final Counters.Counter combineInputCounter = reporter
				.getCounter(TaskCounter.COMBINE_INPUT_RECORDS);
		combinerRunner = CombinerRunner.create(job, getTaskID(),
				combineInputCounter, reporter, null);
		if (combinerRunner == null) {
			combineCollector = null;
		} else {
			final Counters.Counter combineOutputCounter = reporter
					.getCounter(TaskCounter.COMBINE_OUTPUT_RECORDS);
			combineCollector = new CombineOutputCollector<K, V>(
					combineOutputCounter, reporter, job);
		}

	}

	@Override
	public void collect(final K key, final V value, final int partition)
			throws IOException, InterruptedException {
		reporter.progress();
		Timer.checkIn();

		if (key.getClass() != keyClass) {
			throw new IOException("Type mismatch in key from map: expected "
					+ keyClass.getName() + ", received "
					+ key.getClass().getName());
		}
		if (value.getClass() != valClass) {
			throw new IOException("Type mismatch in value from map: expected "
					+ valClass.getName() + ", received "
					+ value.getClass().getName());
		}
		if (partition < 0 || partition >= partitions) {
			throw new IOException("Illegal partition for " + key + " ("
					+ partition + ")");
		}

		try {
			knuthBuffer.add(key, value, partition);
			mapOutputRecordCounter.increment(1);

		} catch (final Exception e) {
			LOG.info("Exception in replacement selection buffer: "
					+ e.getMessage());
		}

	}

	@Override
	public void close() throws IOException, InterruptedException {
	}

	@Override
	public void flush() throws IOException, InterruptedException,
			ClassNotFoundException {
		LOG.info("Starting flush of map output");

		numSpills = knuthBuffer.flush();
		indexCacheList = knuthBuffer.getIndexCacheList();
		Timer.checkOut("Run Generation: ");

		knuthBuffer = null;

		Timer.checkIn();
		mergeParts();
		Timer.checkOut("Merge: ");
		printCounters();

		LOG.info("MapOutputHeap " + mapOutputRecordCounter.getCounter()
				+ " records.");
		final Path outputPath = mapOutputFile.getOutputFile();
		fileOutputByteCounter.increment(rfs.getFileStatus(outputPath).getLen());

	}

	private void mergeParts() throws IOException, InterruptedException,
			ClassNotFoundException {
		// get the approximate size of the final output/index files
		long finalOutFileSize = 0;
		long finalIndexFileSize = 0;
		final Path[] filename = new Path[numSpills];
		final TaskAttemptID mapId = getTaskID();

		for (int i = 0; i < numSpills; i++) {
			filename[i] = mapOutputFile.getSpillFile(i);
			finalOutFileSize += rfs.getFileStatus(filename[i]).getLen();
		}

		if (numSpills == 1) { // the spill is the final output
			rfs.rename(filename[0], new Path(filename[0].getParent(),
					"file.out"));
			if (indexCacheList.size() == 0) {
				rfs.rename(mapOutputFile.getSpillIndexFile(0), new Path(
						filename[0].getParent(), "file.out.index"));
			} else {
				indexCacheList.get(0).writeToFile(
						new Path(filename[0].getParent(), "file.out.index"),
						job);
			}
			sortPhase.complete();
			return;
		}

		// read in paged indices
		for (int i = indexCacheList.size(); i < numSpills; ++i) {
			final Path indexFileName = mapOutputFile.getSpillIndexFile(i);
			indexCacheList.add(new SpillRecord(indexFileName, job));
		}

		// make correction in the length to include the sequence file header
		// lengths for each partition
		finalOutFileSize += partitions * APPROX_HEADER_LENGTH;
		finalIndexFileSize = partitions * MAP_OUTPUT_INDEX_RECORD_LENGTH;
		final Path finalOutputFile = mapOutputFile
				.getOutputFileForWrite(finalOutFileSize);
		final Path finalIndexFile = mapOutputFile
				.getOutputIndexFileForWrite(finalIndexFileSize);

		// The output stream for the final single output file
		final FSDataOutputStream finalOut = rfs.create(finalOutputFile, true,
				4096);

		if (numSpills == 0) {
			// create dummy files
			final IndexRecord rec = new IndexRecord();
			final SpillRecord sr = new SpillRecord(partitions);
			try {
				for (int i = 0; i < partitions; i++) {
					final long segmentStart = finalOut.getPos();
					final Writer<K, V> writer = new Writer<K, V>(job, finalOut,
							keyClass, valClass, codec, null);
					writer.close();
					rec.startOffset = segmentStart;
					rec.rawLength = writer.getRawLength();
					rec.partLength = writer.getCompressedLength();
					sr.putIndex(rec, i);
				}
				sr.writeToFile(finalIndexFile, job);
			} finally {
				finalOut.close();
			}
			sortPhase.complete();
			return;
		}
		{
			sortPhase.addPhases(partitions); // Divide sort phase into
			// sub-phases

			final IndexRecord rec = new IndexRecord();
			final SpillRecord spillRec = new SpillRecord(partitions);
			for (int parts = 0; parts < partitions; parts++) {
				// create the segments to be merged
				final List<Segment<K, V>> segmentList = new ArrayList<Segment<K, V>>(
						numSpills);
				for (int i = 0; i < numSpills; i++) {
					final IndexRecord indexRecord = indexCacheList.get(i)
							.getIndex(parts);

					final Segment<K, V> s = new Segment<K, V>(job, rfs,
							filename[i], indexRecord.startOffset,
							indexRecord.partLength, codec, true);
					segmentList.add(i, s);

					if (LOG.isDebugEnabled()) {
						LOG.debug("MapId=" + mapId + " Reducer=" + parts
								+ "Spill =" + i + "(" + indexRecord.startOffset
								+ "," + indexRecord.rawLength + ", "
								+ indexRecord.partLength + ")");
					}

				}
				final int mergeFactor = job.getInt(MRJobConfig.IO_SORT_FACTOR,
						100);
				// sort the segments only if there are intermediate merges
				final boolean sortSegments = segmentList.size() > mergeFactor;
				// merge
				@SuppressWarnings("unchecked")
				final RawKeyValueIterator kvIter = Merger.merge(job, rfs,
						keyClass, valClass, codec, segmentList, mergeFactor,
						new Path(mapId.toString()),
						job.getOutputKeyComparator(), reporter, sortSegments,
						null, spilledRecordsCounter, sortPhase.phase(),
						TaskType.MAP);

				// write merged output to disk
				final long segmentStart = finalOut.getPos();
				final Writer<K, V> writer = new Writer<K, V>(job, finalOut,
						keyClass, valClass, codec, spilledRecordsCounter);
				if (combinerRunner == null || numSpills < minSpillsForCombine) {
					Merger.writeFile(kvIter, writer, reporter, job);
				} else {
					combineCollector.setWriter(writer);
					combinerRunner.combine(kvIter, combineCollector);
				}

				// close
				writer.close();

				sortPhase.startNextPhase();

				// record offsets
				rec.startOffset = segmentStart;
				rec.rawLength = writer.getRawLength();
				rec.partLength = writer.getCompressedLength();
				spillRec.putIndex(rec, parts);
			}
			spillRec.writeToFile(finalIndexFile, job);
			finalOut.close();

			// do not delete spills, we use them to count the number of runs
			// later
			/*
			 * for (int i = 0; i < numSpills; i++) { rfs.delete(filename[i],
			 * true); }
			 */
		}

	}

	public void printCounters() {
		System.out.println(mapOutputRecordCounter.getDisplayName() + ": "
				+ mapOutputRecordCounter.getValue());
		System.out.println(fileOutputByteCounter.getDisplayName() + ": "
				+ fileOutputByteCounter.getValue());

	}

	private TaskAttemptID getTaskID() {
		return mapTask.getTaskID();
	}

}
