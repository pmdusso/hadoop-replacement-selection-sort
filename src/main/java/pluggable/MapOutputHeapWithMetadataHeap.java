package pluggable;

import io.RunOutputWriterWithOriginalWriter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import memorymanagement.ManageWithLinkedList;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.io.serializer.SerializationFactory;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapred.IFile.Writer;
import org.apache.hadoop.mapred.IndexRecord;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapOutputCollector;
import org.apache.hadoop.mapred.MapOutputFile;
import org.apache.hadoop.mapred.MapTask;
import org.apache.hadoop.mapred.Merger;
import org.apache.hadoop.mapred.Merger.Segment;
import org.apache.hadoop.mapred.RawKeyValueIterator;
import org.apache.hadoop.mapred.SpillRecord;
import org.apache.hadoop.mapred.Task.CombineOutputCollector;
import org.apache.hadoop.mapred.Task.CombinerRunner;
import org.apache.hadoop.mapred.Task.TaskReporter;
import org.apache.hadoop.mapred.TaskAttemptID;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.TaskCounter;
import org.apache.hadoop.mapreduce.TaskType;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.Progress;
import org.apache.hadoop.util.ReflectionUtils;

import replacementselection.Timer;

public class MapOutputHeapWithMetadataHeap<K extends Object, V extends Object>
		implements MapOutputCollector<K, V>, IndexedSortable {

	private static final int MAX_RECORD_SIZE = 1024;

	private static final Log LOG = LogFactory
			.getLog(MapOutputHeapWithMetadataHeap.class.getName());

	private int partitions;
	private JobConf job;
	private TaskReporter reporter;
	private Class<K> keyClass;
	private Class<V> valClass;
	private RawComparator<K> comparator;
	private SerializationFactory serializationFactory;
	private Serializer<K> keySerializer;
	private Serializer<V> valSerializer;
	final BlockingBuffer bb = new BlockingBuffer();

	private CombinerRunner<K, V> combinerRunner;
	private CombineOutputCollector<K, V> combineCollector;

	// Compression for map-outputs
	private CompressionCodec codec = null;

	// Replacement Selection
	private RunOutputWriterWithOriginalWriter<K, V> originalOutputWriter;

	// Metadata
	private static final int keyPrefixLength = 8;
	private byte[] transitionBuffer;
	private byte[] lastSpilledRecordKey;
	private int lastSpilledRecordKeyLength;

	private byte[] lastSpilledRecord;
	private byte[] incomingRecord;
	private ByteBuffer bbIncomingRecord;
	private boolean isAnyRecordSpilled = false;

	private ManageWithLinkedList theManager;
	private MetadataHeap metadata;
	private int currentRun;
	public int writtenBytesSoFar;

	// Buffer used to output key and value
	private DataInputBuffer keyBuffer;
	private DataInputBuffer valueBuffer;

	private FileSystem rfs;
	private int numSpills = 0;

	private Counters.Counter mapOutputByteCounter;
	private Counters.Counter mapOutputRecordCounter;
	private Counters.Counter fileOutputByteCounter;

	private Counters.Counter memoryResetRsCounter;
	private Counters.Counter mapOutputBlockRecordByteCounter;
	private Counters.Counter decidedWithKeyPrefix;
	private Counters.Counter decidedWithFullKey;
	private Counters.Counter recordUndecided;

	private MapTask mapTask;
	private MapOutputFile mapOutputFile;
	private Progress sortPhase;
	private Counters.Counter spilledRecordsCounter;
	private ArrayList<SpillRecord> indexCacheList;

	private int minSpillsForCombine;

	private byte[] kvbuffer; // main output buffer
	private int extentSize; // size of the extents which divide the manage
							// memory
	private int minBlockSize;
	private int kvbufferSize;

	private static final int METASIZE = 32;

	private static final int APPROX_HEADER_LENGTH = 150;
	private static final int MAP_OUTPUT_INDEX_RECORD_LENGTH = 24;

	@Override
	public int compare(final int i, final int j) {
		throw new NotImplementedException();
	}

	@Override
	public void swap(final int i, final int j) {
		throw new NotImplementedException();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init(
			final org.apache.hadoop.mapred.MapOutputCollector.Context context)
			throws IOException, ClassNotFoundException {
		job = context.getJobConf();
		reporter = context.getReporter();
		mapTask = context.getMapTask();
		mapOutputFile = mapTask.getMapOutputFile();
		sortPhase = mapTask.getSortPhase();
		spilledRecordsCounter = reporter
				.getCounter(TaskCounter.SPILLED_RECORDS);
		partitions = job.getNumReduceTasks();
		rfs = FileSystem.getLocal(job).getRaw();

		indexCacheList = new ArrayList<SpillRecord>();

		// sanity checks
		final float spillper = job.getFloat("io.sort.spill.percent",
				(float) 0.8);
		final float recper = job.getFloat("io.sort.record.percent",
				(float) 0.05);
		final int sortmb = job.getInt("io.sort.mb", 100);
		if (spillper > (float) 1.0 || spillper < (float) 0.0) {
			throw new IOException("Invalid \"io.sort.spill.percent\": "
					+ spillper);
		}
		if (recper > (float) 1.0 || recper < (float) 0.01) {
			throw new IOException("Invalid \"io.sort.record.percent\": "
					+ recper);
		}
		if ((sortmb & 0x7FF) != sortmb) {
			throw new IOException("Invalid \"io.sort.mb\": " + sortmb);
		}

		LOG.info("io.sort.mb = " + sortmb);

		// buffers and accounting
		lastSpilledRecord = new byte[MetadataHeap.HEAP_ENTRY_LENGTH];
		lastSpilledRecordKey = new byte[256];
		lastSpilledRecordKeyLength = 0;

		incomingRecord = new byte[MetadataHeap.HEAP_ENTRY_LENGTH];
		bbIncomingRecord = ByteBuffer.wrap(incomingRecord);

		// k/v serialization
		comparator = job.getOutputKeyComparator();
		keyClass = (Class<K>) job.getMapOutputKeyClass();
		valClass = (Class<V>) job.getMapOutputValueClass();
		serializationFactory = new SerializationFactory(job);
		keySerializer = serializationFactory.getSerializer(keyClass);
		keySerializer.open(bb);
		valSerializer = serializationFactory.getSerializer(valClass);
		valSerializer.open(bb);

		// spill
		int maxMemUsage = sortmb << 20;
		maxMemUsage -= maxMemUsage % METASIZE;
		extentSize = 8192;
		minBlockSize = 64;
		kvbufferSize = maxMemUsage / extentSize * extentSize;
		kvbuffer = new byte[kvbufferSize];
		LOG.info("MapOutputHeap buffer = " + kvbufferSize);
		originalOutputWriter = new RunOutputWriterWithOriginalWriter<K, V>(
				mapOutputFile, false, rfs, job, keyClass, valClass, codec,
				spilledRecordsCounter);

		// Replacement Selection
		transitionBuffer = new byte[MAX_RECORD_SIZE];
		theManager = new ManageWithLinkedList(kvbufferSize / extentSize,
				extentSize, minBlockSize);
		int maxSize = ((kvbufferSize / METASIZE) * 3);
		metadata = new MetadataHeap(maxSize);
		LOG.info("MapOutputHeap metadata (in records) = " + maxSize);
		currentRun = 0;
		writtenBytesSoFar = 0;

		// Write buffers
		keyBuffer = new DataInputBuffer();
		valueBuffer = new DataInputBuffer();

		// counters
		mapOutputByteCounter = reporter
				.getCounter(TaskCounter.MAP_OUTPUT_BYTES);
		mapOutputRecordCounter = reporter
				.getCounter(TaskCounter.MAP_OUTPUT_RECORDS);
		fileOutputByteCounter = reporter
				.getCounter(TaskCounter.MAP_OUTPUT_MATERIALIZED_BYTES);

		memoryResetRsCounter = reporter
				.getCounter(ReplacementSelectionCounter.MEMORY_RESET);
		mapOutputBlockRecordByteCounter = reporter
				.getCounter(ReplacementSelectionCounter.MEMORY_BLOCK_RECORD_SIZE);
		decidedWithKeyPrefix = reporter
				.getCounter(ReplacementSelectionCounter.DECIDED_WITH_KEY_PREFIX);
		decidedWithFullKey = reporter
				.getCounter(ReplacementSelectionCounter.DECIDED_WITH_FULL_KEY);
		recordUndecided = reporter
				.getCounter(ReplacementSelectionCounter.UNDECIDED);

		// compression
		if (job.getCompressMapOutput()) {
			final Class<? extends CompressionCodec> codecClass = job
					.getMapOutputCompressorClass(DefaultCodec.class);
			codec = ReflectionUtils.newInstance(codecClass, job);
		} else {
			codec = null;
		}

		// combiner
		minSpillsForCombine = job.getInt(MRJobConfig.MAP_COMBINE_MIN_SPILLS, 3);
		final Counters.Counter combineInputCounter = reporter
				.getCounter(TaskCounter.COMBINE_INPUT_RECORDS);
		combinerRunner = CombinerRunner.create(job, getTaskID(),
				combineInputCounter, reporter, null);
		if (combinerRunner != null) {
			final Counters.Counter combineOutputCounter = reporter
					.getCounter(TaskCounter.COMBINE_OUTPUT_RECORDS);
			combineCollector = new CombineOutputCollector<K, V>(
					combineOutputCounter, reporter, job);
		} else {
			combineCollector = null;
		}

	}

	@Override
	public void collect(final K key, final V value, final int partition)
			throws IOException, InterruptedException {
		reporter.progress();
		Timer.checkIn();

		if (key.getClass() != keyClass) {
			throw new IOException("Type mismatch in key from map: expected "
					+ keyClass.getName() + ", recieved "
					+ key.getClass().getName());
		}
		if (value.getClass() != valClass) {
			throw new IOException("Type mismatch in value from map: expected "
					+ valClass.getName() + ", recieved "
					+ value.getClass().getName());
		}

		if (partition < 0 || partition >= partitions) {
			throw new IOException("Illegal partition for " + key + " ("
					+ partition + ")");
		}

		// We execute the loop until we add the current key/value pair into the
		// buffer. It may be necessary to remove/output/spill several records
		// from the buffer until we can add the current one. We serialize first
		// the key and value in a temporary small buffer

		writtenBytesSoFar = 0;
		keySerializer.serialize(key);
		final int keyLength = writtenBytesSoFar;

		valSerializer.serialize(value);
		final int recordLength = writtenBytesSoFar;

		while (true) {
			// sufficient buffer space?
			final long memoryBlock = theManager.grabMemoryLong(recordLength);
			if (memoryBlock > -1) {
				// here, we know that we have sufficient space to write
				System.arraycopy(transitionBuffer, 0, kvbuffer,
						(int) (memoryBlock >> 32), (int) memoryBlock);
				addToSelectionTree(partition, keyLength, recordLength,
						memoryBlock);
				break;
			}
			flushFromSelectionTree(false, true);
		}
	}

	/**
	 * Input process that fill memory with new records and adds them to the
	 * selection tree. The last spilled record can be null when no record has
	 * been spilled yet. In this case, we add the incoming record directly to
	 * the selection tree. If the last spilled record is bigger than the
	 * incoming record, the incoming record cannot belong to the current run
	 * anymore. Thus, we add it with a run number currentRun + 1
	 * */
	private void addToSelectionTree(final int partition, final int keyLength,
			final int recordLength, final long memoryBlock) {

		bbIncomingRecord.position(MetadataHeap.PARTITION);
		bbIncomingRecord.putInt(partition);
		bbIncomingRecord.put(transitionBuffer, 1, keyPrefixLength);
		bbIncomingRecord.putInt(keyLength);
		bbIncomingRecord.putInt(recordLength);
		int memoryBlockAddress = (int) (memoryBlock >> 32);
		bbIncomingRecord.putInt(memoryBlockAddress);
		bbIncomingRecord.putInt((int) memoryBlock);

		int newRun = -1;

		if (isAnyRecordSpilled != false) {
			if (compareLastRecordToIncomingRecord(memoryBlockAddress, keyLength) > 0) {
				newRun = currentRun + 1;
			} else {
				newRun = currentRun;
			}
		} else {
			newRun = currentRun;
		}

		bbIncomingRecord.putInt(MetadataHeap.RUN, newRun);

		if (metadata.isFull()) {
			metadata.grow(metadata.getHeap().length);
		}

		metadata.put(incomingRecord);
	}

	/**
	 * We compare the incoming record to the last spilled record to decide if
	 * the incoming one should have the same run number as the last spilled one.
	 * 
	 * We compare the partition, the key prefix and finally the full key, if it
	 * is necessary.
	 * 
	 * @param incomingBlockAddress
	 * @param incomingRecordLength
	 * */
	private int compareLastRecordToIncomingRecord(
			final int incomingBlockAddress, final int incomingKeyLength) {

		int r = WritableComparator.compareBytes(lastSpilledRecord,
				MetadataHeap.PARTITION, MetadataHeap.PARTITION
						+ MetadataHeap.KEY_PREFIX, incomingRecord,
				MetadataHeap.PARTITION, MetadataHeap.PARTITION
						+ MetadataHeap.KEY_PREFIX);

		if (r == 0) {
			// If we cannot decide using only the partition and the key prefix,
			// we need to compare the whole keys
			r = comparator.compare(lastSpilledRecordKey, 0,
					lastSpilledRecordKeyLength, kvbuffer, incomingBlockAddress,
					incomingKeyLength);
		}

		return r;
	}

	/**
	 * Whenever it fails to find memory space for an incoming record, it resumes
	 * the output process, which runs until it has created a free slot of
	 * sufficient size.
	 * 
	 * @param isFinalFlush
	 *            : we keep control of the final flush because, that case, we do
	 *            not have to restore memory anymore - which saves us some
	 *            instructions.
	 * */
	private void flushFromSelectionTree(final boolean isFinalFlush,
			final boolean isBatchFlush) {
		isAnyRecordSpilled = true;

		if (isBatchFlush && metadata.size() > 1000) {
			for (int i = 0; i < 1000; i++) {
				flushToWriter(false);
			}
		} else {
			flushToWriter(false);
		}
	}

	/**
	 * If the run of the smallest record is bigger than the value of the current
	 * run, that means that there are no records belonging to this (current) run
	 * anymore. We should close this run and start the next one.
	 * 
	 * We do not restore memory in the final flush because there won't be any
	 * new incoming records.
	 * */
	private void flushToWriter(final boolean isFinalFlush) {

		if (metadata.isEmpty()) {
			memoryResetRsCounter.increment(1);
			theManager = new ManageWithLinkedList(kvbufferSize / extentSize,
					extentSize, minBlockSize);
			return;
		}

		if (metadata.getIntFieldAtTheTop(MetadataHeap.RUN) > currentRun) {
			originalOutputWriter.reset();
			currentRun = metadata.getIntFieldAtTheTop(MetadataHeap.RUN);
		}

		if (!isFinalFlush) {
			retoreMemoryAndSaveKey(
					metadata.getIntFieldAtTheTop(MetadataHeap.BLOCK_ADDRESS),
					metadata.getIntFieldAtTheTop(MetadataHeap.BLOCK_LENGTH),
					metadata.getIntFieldAtTheTop(MetadataHeap.KEY_LENGTH));
		}

		writeToSpill(metadata.getIntFieldAtTheTop(MetadataHeap.PARTITION),
				metadata.getIntFieldAtTheTop(MetadataHeap.KEY_LENGTH),
				metadata.getIntFieldAtTheTop(MetadataHeap.RECORD_LENGTH),
				metadata.getIntFieldAtTheTop(MetadataHeap.BLOCK_ADDRESS),
				metadata.getIntFieldAtTheTop(MetadataHeap.BLOCK_LENGTH));

		lastSpilledRecord = metadata.pop();
	}

	/**
	 * Return the free memory block to the memory manager. Also, save the
	 * (serialized) key of the last spilled record to use in a raw comparison
	 * 
	 * @param blockAddress
	 *            : the address of the memory block in the kvbuffer
	 * @param blockLength
	 *            : the length of the block, based on the unity of the memory
	 *            manager (32, 64, ...)
	 * @param keyLength
	 *            : the length of just the key
	 * 
	 * */
	private void retoreMemoryAndSaveKey(final int blockAddress,
			final int blockLength, final int keyLength) {

		// Restore
		theManager.restoreMemoryBlock(blockAddress, blockLength);
		// Save key and key length
		if (keyLength > lastSpilledRecordKey.length)
			lastSpilledRecordKey = new byte[lastSpilledRecordKey.length * 2];
		System.arraycopy(kvbuffer, blockAddress, lastSpilledRecordKey, 0,
				keyLength);
		lastSpilledRecordKeyLength = keyLength;

	}

	private void writeToSpill(final int partition, final int keyLenght,
			final int recordLength, final int blockAddress, int blockLength) {

		keyBuffer.reset(kvbuffer, blockAddress, keyLenght);
		valueBuffer.reset(kvbuffer, blockAddress + keyLenght, recordLength
				- keyLenght);

		mapOutputRecordCounter.increment(1);
		mapOutputByteCounter.increment(recordLength);
		mapOutputBlockRecordByteCounter.increment(blockLength);

		originalOutputWriter.write(keyBuffer, valueBuffer, partition);
	}

	@Override
	public void flush() throws IOException, InterruptedException,
			ClassNotFoundException {
		LOG.info("Starting flush of map output");

		while (metadata.size() > 0) {
			flushToWriter(true);
		}

		Timer.checkOut("Run generation: ");

		originalOutputWriter.flush();
		numSpills = originalOutputWriter.getNumSpills();

		metadata = null;
		kvbuffer = null;
		transitionBuffer = null;
		theManager = null;
		indexCacheList = originalOutputWriter.getIndexCacheList();

		Timer.checkIn();
		mergeParts();
		Timer.checkOut("Merge: ");
		printCounters();

		LOG.info("MapOutputHeap wrote " + mapOutputRecordCounter.getCounter()
				+ " records.");
		final Path outputPath = mapOutputFile.getOutputFile();
		fileOutputByteCounter.increment(rfs.getFileStatus(outputPath).getLen());

	}

	@Override
	public void close() throws IOException, InterruptedException {

	}

	private TaskAttemptID getTaskID() {
		return mapTask.getTaskID();
	}

	private void mergeParts() throws IOException, InterruptedException,
			ClassNotFoundException {
		// get the approximate size of the final output/index files
		long finalOutFileSize = 0;
		long finalIndexFileSize = 0;
		final Path[] filename = new Path[numSpills];
		final TaskAttemptID mapId = getTaskID();

		for (int i = 0; i < numSpills; i++) {
			filename[i] = mapOutputFile.getSpillFile(i);
			finalOutFileSize += rfs.getFileStatus(filename[i]).getLen();
		}

		if (numSpills == 1) { // the spill is the final output
			rfs.rename(filename[0], new Path(filename[0].getParent(),
					"file.out"));
			if (indexCacheList.size() == 0) {
				rfs.rename(mapOutputFile.getSpillIndexFile(0), new Path(
						filename[0].getParent(), "file.out.index"));
			} else {
				indexCacheList.get(0).writeToFile(
						new Path(filename[0].getParent(), "file.out.index"),
						job);
			}
			sortPhase.complete();
			return;
		}

		// read in paged indices
		for (int i = indexCacheList.size(); i < numSpills; ++i) {
			final Path indexFileName = mapOutputFile.getSpillIndexFile(i);
			indexCacheList.add(new SpillRecord(indexFileName, job));
		}

		// make correction in the length to include the sequence file header
		// lengths for each partition
		finalOutFileSize += partitions * APPROX_HEADER_LENGTH;
		finalIndexFileSize = partitions * MAP_OUTPUT_INDEX_RECORD_LENGTH;
		final Path finalOutputFile = mapOutputFile
				.getOutputFileForWrite(finalOutFileSize);
		final Path finalIndexFile = mapOutputFile
				.getOutputIndexFileForWrite(finalIndexFileSize);

		// The output stream for the final single output file
		final FSDataOutputStream finalOut = rfs.create(finalOutputFile, true,
				4096);

		if (numSpills == 0) {
			// create dummy files
			final IndexRecord rec = new IndexRecord();
			final SpillRecord sr = new SpillRecord(partitions);
			try {
				for (int i = 0; i < partitions; i++) {
					final long segmentStart = finalOut.getPos();
					final Writer<K, V> writer = new Writer<K, V>(job, finalOut,
							keyClass, valClass, codec, null);
					writer.close();
					rec.startOffset = segmentStart;
					rec.rawLength = writer.getRawLength();
					rec.partLength = writer.getCompressedLength();
					sr.putIndex(rec, i);
				}
				sr.writeToFile(finalIndexFile, job);
			} finally {
				finalOut.close();
			}
			sortPhase.complete();
			return;
		}
		{
			sortPhase.addPhases(partitions); // Divide sort phase into
			// sub-phases

			final IndexRecord rec = new IndexRecord();
			final SpillRecord spillRec = new SpillRecord(partitions);
			for (int parts = 0; parts < partitions; parts++) {
				// create the segments to be merged
				final List<Segment<K, V>> segmentList = new ArrayList<Segment<K, V>>(
						numSpills);
				for (int i = 0; i < numSpills; i++) {
					final IndexRecord indexRecord = indexCacheList.get(i)
							.getIndex(parts);

					final Segment<K, V> s = new Segment<K, V>(job, rfs,
							filename[i], indexRecord.startOffset,
							indexRecord.partLength, codec, true);
					segmentList.add(i, s);

					// if (LOG.isDebugEnabled()) {
					LOG.info("MapId=" + mapId + " Reducer=" + parts + "Spill ="
							+ i + "(" + indexRecord.startOffset + ","
							+ indexRecord.rawLength + ", "
							+ indexRecord.partLength + ")");
					// }

				}
				final int mergeFactor = job.getInt(MRJobConfig.IO_SORT_FACTOR,
						100);
				// sort the segments only if there are intermediate merges
				final boolean sortSegments = segmentList.size() > mergeFactor;
				// merge
				@SuppressWarnings("unchecked")
				final RawKeyValueIterator kvIter = Merger.merge(job, rfs,
						keyClass, valClass, codec, segmentList, mergeFactor,
						new Path(mapId.toString()),
						job.getOutputKeyComparator(), reporter, sortSegments,
						null, spilledRecordsCounter, sortPhase.phase(),
						TaskType.MAP);

				// write merged output to disk
				final long segmentStart = finalOut.getPos();
				final Writer<K, V> writer = new Writer<K, V>(job, finalOut,
						keyClass, valClass, codec, spilledRecordsCounter);
				if (combinerRunner == null || numSpills < minSpillsForCombine) {
					Merger.writeFile(kvIter, writer, reporter, job);
				} else {
					combineCollector.setWriter(writer);
					combinerRunner.combine(kvIter, combineCollector);
				}

				// close
				writer.close();

				sortPhase.startNextPhase();

				// record offsets
				rec.startOffset = segmentStart;
				rec.rawLength = writer.getRawLength();
				rec.partLength = writer.getCompressedLength();
				spillRec.putIndex(rec, parts);
			}
			spillRec.writeToFile(finalIndexFile, job);
			finalOut.close();

			// do not delete spills, we use them to count the number of runs
			// later
			/*
			 * for (int i = 0; i < numSpills; i++) { rfs.delete(filename[i],
			 * true); }
			 */
		}

	}

	public void printCounters() {
		System.out.println(mapOutputRecordCounter.getDisplayName() + ": "
				+ mapOutputRecordCounter.getValue());
		System.out.println(mapOutputByteCounter.getDisplayName() + ": "
				+ mapOutputByteCounter.getValue());
		System.out.println(fileOutputByteCounter.getDisplayName() + ": "
				+ fileOutputByteCounter.getValue());

		System.out.println(memoryResetRsCounter.getDisplayName() + ": "
				+ memoryResetRsCounter.getValue());
		System.out.println(mapOutputBlockRecordByteCounter.getDisplayName()
				+ ": " + mapOutputBlockRecordByteCounter.getValue());

		System.out.println(decidedWithKeyPrefix.getDisplayName() + ": "
				+ decidedWithKeyPrefix.getValue());
		System.out.println(decidedWithFullKey.getDisplayName() + ": "
				+ decidedWithFullKey.getValue());
		System.out.println(recordUndecided.getDisplayName() + ": "
				+ recordUndecided.getValue());
	}

	/**********************************************************************************************
	 * INNER CLASSES
	 **********************************************************************************************/

	/*
	 * Inner class managing the spill of serialized records to disk.
	 */
	protected class BlockingBuffer extends DataOutputStream {

		public BlockingBuffer() {
			this(new Buffer());
		}

		private BlockingBuffer(final OutputStream out) {
			super(out);
		}

		/**
		 * Mark end of record. Note that this is required if the buffer is to
		 * cut the spill in the proper place.
		 */
		public int markRecord() {
			return 0;
		}

		protected synchronized void reset() throws IOException {
			throw new NotImplementedException();
		}
	}

	public class Buffer extends OutputStream {

		private final byte[] scratch = new byte[1];

		@Override
		public synchronized void write(final int v) throws IOException {
			scratch[0] = (byte) v;
			write(scratch, 0, 1);
		}

		/**
		 * Attempt to write a sequence of bytes to the transition buffer.
		 */
		@Override
		public synchronized void write(final byte b[], final int off,
				final int len) throws IOException {

			if (len <= MAX_RECORD_SIZE) {
				System.arraycopy(b, off, transitionBuffer, writtenBytesSoFar,
						len);
				writtenBytesSoFar += len;
			} else {
				throw new RuntimeException(
						"Key or value does not fit in the transition buffer");
			}
		}
	}

	class MetadataHeap {

		public static final int RUN = 0;
		public static final int PARTITION = RUN + 4;
		public static final int KEY_PREFIX = PARTITION + 4;
		public static final int KEY_LENGTH = KEY_PREFIX + keyPrefixLength;
		public static final int RECORD_LENGTH = KEY_LENGTH + 4;
		public static final int BLOCK_ADDRESS = RECORD_LENGTH + 4;
		public static final int BLOCK_LENGTH = BLOCK_ADDRESS + 4;

		public static final int RUN_PARTITION_KEY_PREFIX_LENGTH = KEY_PREFIX
				+ keyPrefixLength;
		public static final int HEAP_ENTRY_LENGTH = BLOCK_LENGTH + 4;

		private final byte[] empty = new byte[HEAP_ENTRY_LENGTH];
		private final byte[] node = new byte[HEAP_ENTRY_LENGTH];
		private final byte[] temp = new byte[HEAP_ENTRY_LENGTH];
		private final byte[] result = new byte[HEAP_ENTRY_LENGTH];

		private byte[] heap;
		private ByteBuffer bbufer;
		private int size;
		private int maxSize;

		public MetadataHeap(final int maxSize) {
			Arrays.fill(empty, (byte) 0);
			size = 0;
			final int heapSize = maxSize + 1;
			heap = new byte[heapSize * HEAP_ENTRY_LENGTH];
			bbufer = ByteBuffer.wrap(heap);
			this.maxSize = maxSize;
		}

		/**
		 * Increase the size of the current heap in newPositions' records.
		 * */
		public void grow(final int newPositions) {
			// Save the current heap in a temp array
			byte[] temp = new byte[heap.length];
			System.arraycopy(heap, 0, temp, 0, heap.length);

			// Instantiate a new heap, with more space
			final int heapSize = maxSize + newPositions + 1;
			heap = new byte[heapSize * HEAP_ENTRY_LENGTH];

			// Copy back the previously content of the heap
			System.arraycopy(temp, 0, heap, 0, temp.length);

			bbufer = ByteBuffer.wrap(heap);
			temp = null;
			this.maxSize = maxSize + newPositions;
		}

		public boolean isFull() {
			if (size == maxSize) {
				return true;
			} else {
				return false;
			}
		}

		public boolean isEmpty() {
			if (size == 0) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Adds an Object to a PriorityQueue in log(size) time. If one tries to
		 * add more objects than maxSize from initialize a RuntimeException
		 * (ArrayIndexOutOfBound) is thrown.
		 */
		public final boolean put(final byte[] element) {
			if (size + 1 <= maxSize) {
				size++;
				goToPosition(size);
				bbufer.put(element);
				upHeap();
				return true;
			} else {
				return false;
			}
		}

		/** Returns the least element of the PriorityQueue in constant time. */
		public final byte[] top() {
			if (size > 0) {
				goToPosition(1);
				bbufer.get(result, 0, HEAP_ENTRY_LENGTH);
				return result;
			} else {
				return null;
			}
		}

		public final int getIntFieldAtTheTop(final int fieldOffset) {
			if (size > 0) {
				return bbufer.getInt(HEAP_ENTRY_LENGTH + fieldOffset);
			} else {
				return -1;
			}
		}

		/**
		 * Removes and returns the least element of the PriorityQueue in
		 * log(size) time.
		 */

		public final byte[] pop() {
			if (size > 0) {
				goToPosition(1);
				bbufer.get(result, 0, HEAP_ENTRY_LENGTH);// save first value

				install(1, size); // move last to first

				goToPosition(size); // permit GC of objects
				bbufer.put(empty);

				size--;
				downHeap(); // adjust heap
				return result;
			} else {
				return null;
			}
		}

		/**
		 * We have one record at each HEAP_ENTRY_LENGTH bytes
		 */
		private int goToPosition(final int p) {
			bbufer.position(p * HEAP_ENTRY_LENGTH);
			return bbufer.position();
		}

		/**
		 * Should be called when the Object at top changes values. Still log(n)
		 * worst case, but it's at least twice as fast to
		 * 
		 * <pre>
		 * {
		 * 	pq.top().change();
		 * 	pq.adjustTop();
		 * }
		 * </pre>
		 * 
		 * instead of
		 * 
		 * <pre>
		 * {
		 * 	o = pq.pop();
		 * 	o.change();
		 * 	pq.push(o);
		 * }
		 * </pre>
		 */
		public final void adjustTop() {
			downHeap();
		}

		/**
		 * Returns the number of elements currently stored in the PriorityQueue.
		 */
		public final int size() {
			return size;
		}

		/** Removes all entries from the PriorityQueue. */
		public final void clear() {
			for (int i = 0; i <= size * HEAP_ENTRY_LENGTH; i++) {
				heap[i] = 0;
			}
			size = 0;
		}

		private final void upHeap() {
			int i = size;
			goToPosition(i);
			bbufer.get(node, 0, HEAP_ENTRY_LENGTH); // save bottom node

			goToPosition(i);
			bbufer.put(empty);

			int j = i >>> 1;

			while (j > 0 && lessThan(node, j)) {
				install(i, j); // shift parents down
				i = j;
				j = j >>> 1;
			}
			goToPosition(i);
			bbufer.put(node); // install saved node
		}

		private final void downHeap() {
			int i = 1;
			goToPosition(i);
			bbufer.get(node, 0, HEAP_ENTRY_LENGTH); // save top node

			int j = i << 1; // find smaller child
			int k = j + 1;
			if (k <= size && lessThan(k, j)) {
				j = k;
			}
			while (j <= size && lessThan(j, node)) {
				install(i, j); // shift up child

				i = j;
				j = i << 1;
				k = j + 1;
				if (k <= size && lessThan(k, j)) {
					j = k;
				}
			}
			goToPosition(i);
			bbufer.put(node); // install saved node
		}

		/**
		 * Get out the "record" in position j and install it at position i
		 * heap[i] = heap[j]
		 * */
		private void install(final int i, final int j) {
			//
			goToPosition(j);
			bbufer.get(temp, 0, HEAP_ENTRY_LENGTH);
			//
			goToPosition(i);
			bbufer.put(temp);
		}

		final byte[] thisOne = new byte[HEAP_ENTRY_LENGTH];
		final byte[] thatOne = new byte[HEAP_ENTRY_LENGTH];

		/**
		 * Determines the ordering of objects in this priority queue. Subclasses
		 * must define this one method.
		 */
		protected boolean lessThan(final byte[] a, final byte[] b) {

			if (lessThanComplete(a, b)) {
				return true;
			} else {
				return false;
			}

		}

		protected boolean lessThan(final int a, final int b) {

			System.arraycopy(heap, a * HEAP_ENTRY_LENGTH, thisOne, 0,
					HEAP_ENTRY_LENGTH);
			System.arraycopy(heap, b * HEAP_ENTRY_LENGTH, thatOne, 0,
					HEAP_ENTRY_LENGTH);

			if (lessThanComplete(thisOne, thatOne)) {
				return true;
			} else {
				return false;
			}

		}

		protected boolean lessThan(final byte[] a, final int b) {

			System.arraycopy(heap, b * HEAP_ENTRY_LENGTH, thisOne, 0,
					HEAP_ENTRY_LENGTH);

			// bbufer.get(thisOne, b * HEAP_ENTRY_LENGTH, HEAP_ENTRY_LENGTH);

			if (lessThanComplete(a, thisOne)) {
				return true;
			} else {
				// >= bigger or equal, because the method checks for less than
				return false;
			}
		}

		protected boolean lessThan(final int a, final byte[] b) {

			System.arraycopy(heap, a * HEAP_ENTRY_LENGTH, thisOne, 0,
					HEAP_ENTRY_LENGTH);

			if (lessThanComplete(thisOne, b)) {
				return true;
			} else {
				return false;
			}
		}

		protected boolean lessThanComplete(final byte[] thisOne,
				final byte[] thatOne) {

			// Where we compare run, partition and key prefix all in one
			// comparison.
			int r = WritableComparator.compareBytes(thisOne, 0,
					RUN_PARTITION_KEY_PREFIX_LENGTH, thatOne, 0,
					RUN_PARTITION_KEY_PREFIX_LENGTH);

			if (r < 0) {
				decidedWithKeyPrefix.increment(1);
				return true;
			}

			// It they all above fields are equal, we need to compare the whole
			// key
			if (r == 0) {
				// obtain the addresses in kvbuffer. We sum the keyPrefixLength
				// so we dont compare it again
				final int thisOneAddress = ByteBuffer.wrap(thisOne).getInt(
						BLOCK_ADDRESS)
						+ keyPrefixLength;
				final int thatOneAddress = ByteBuffer.wrap(thatOne).getInt(
						BLOCK_ADDRESS)
						+ keyPrefixLength;

				final int thisOneLength = ByteBuffer.wrap(thisOne).getInt(
						RECORD_LENGTH)
						- keyPrefixLength;
				final int thatOneLength = ByteBuffer.wrap(thatOne).getInt(
						RECORD_LENGTH)
						- keyPrefixLength;

				r = comparator.compare(kvbuffer, thisOneAddress, thisOneLength,
						kvbuffer, thatOneAddress, thatOneLength);
			}

			if (r < 0) {
				decidedWithFullKey.increment(1);
				return true;
			}

			recordUndecided.increment(1);
			return false;

		}

		public byte[] getHeap() {
			return heap;
		}

		public void print() {
			for (int i = 0; i < maxSize; i++) {
				for (int j = 0; j < HEAP_ENTRY_LENGTH; j++) {

					switch (j) {
					case RUN:
						System.out.print("<");
						break;
					case PARTITION:
						System.out.print("><");
						break;
					case KEY_PREFIX:
						System.out.print("><");
						break;
					case KEY_LENGTH:
						System.out.print("><");
						break;
					case RECORD_LENGTH:
						System.out.print("><");
						break;
					case BLOCK_ADDRESS:
						System.out.print("><");
						break;
					case BLOCK_LENGTH:
						System.out.print("><");
						break;
					default:
						break;
					}
					System.out.print(heap[i * HEAP_ENTRY_LENGTH + j]);
				}
				System.out.println(">");
			}
			System.out.println(System.lineSeparator());
			System.out.println(System.lineSeparator());
		}

		public int getMaxSize() {
			return maxSize;
		}

	}
}
