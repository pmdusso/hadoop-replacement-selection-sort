/**
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license agreements. See the NOTICE
 * file distributed with this work for additional information regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package apache;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.mapred.MapOutputCollector;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.log4j.PropertyConfigurator;

import pluggable.MapOutputHeapWithKeyPrefix;
import pluggable.MapOutputHeapWithMetadataHeap;
import io.serialization.*;

public class WordCount {

    /**
     * A WritableComparator optimized for Text keys.
     */
    public static class Comparator extends WritableComparator {
        public Comparator() {
            super(Text.class);
        }

        @Override
        public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
            final int n1 = WritableUtils.decodeVIntSize(b1[s1]);
            final int n2 = WritableUtils.decodeVIntSize(b2[s2]);
            return compareBytes(b1, s1 + n1, l1 - n1, b2, s2 + n2, l2 - n2);
        }
    }

    public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        private final IntWritable result = new IntWritable();

        @Override
        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for (final IntWritable val : values) {
                sum += val.get();
            }
            result.set(sum);
            context.write(key, result);
        }
    }

    public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private final Text word = new Text();

        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            final StringTokenizer itr = new StringTokenizer(value.toString());
            while (itr.hasMoreTokens()) {
                word.set(itr.nextToken().replaceAll("[^A-Za-z0-9 ]+", ""));
                context.write(word, one);
            }
        }
    }

    private static final String QUEUE = "pluggable.MapOutputQueue";
    private static final String HEAP = "pluggable.MapOutputHeap";
    private static final String KEYPREFIX = "pluggable.MapOutputHeapWithKeyPrefix";
    private static final String METAHEAP = "pluggable.MapOutputHeapWithMetadataHeap";

    public static void main(String[] args) throws Exception {

        MapOutputHeapWithKeyPrefix<Object, Object> mm;
        MapOutputHeapWithMetadataHeap<Object, Object> m;

        int r = 0;
        r = gettingJobDone(args, "");
        // r = gettingJobDone(args, QUEUE);
        // r = gettingJobDone(args, HEAP);
        // r = gettingJobDone(args, KEYPREFIX);
        // r = gettingJobDone(args, METAHEAP);

        System.exit(r);
    }

    private static int gettingJobDone(String[] args, String outputBuffer) throws IOException, InterruptedException, ClassNotFoundException {
        PropertyConfigurator.configure("log4j.properties");
        final Configuration conf = new Configuration();
        final String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        System.out.println("Testing custom output...");
        if (otherArgs.length != 2) {
            System.err.println("Usage: wordcount <in> <out>");
            System.out.println("Usage: wordcount <in> <out>");
            System.exit(2);
        }


        //conf.set("io.serializations", "io.serialization.WritableSerializationWithZeroEndingText");
        //conf.set("mapreduce.job.map.output.collector.class", "pluggable.MapOutputHeapWithMetadataHeap");

	/*
     * Path outputDir = null; String string = otherArgs[3]; switch (string) { case QUEUE: outputDir = new
	 * Path(otherArgs[1] + "out_queue/"); conf.set("mapreduce.job.map.output.collector.class", otherArgs[3]); break;
	 * case HEAP: outputDir = new Path(otherArgs[1] + "out_heap/");
	 * conf.set("mapreduce.job.map.output.collector.class", otherArgs[3]); break; case KEYPREFIX: outputDir = new
	 * Path(otherArgs[1] + "out_keyprefix/"); conf.set("mapreduce.job.map.output.collector.class", otherArgs[3]);
	 * break; case METAHEAP: outputDir = new Path(otherArgs[1] + "out_metaheap/");
	 * conf.set("mapreduce.job.map.output.collector.class", otherArgs[3]); break; case "": outputDir = new
	 * Path(otherArgs[1] + "out_original/"); break; default: break; }
	 */

        //conf.setClass("pluggable.MapOutputHeapWithMetadataHeap", MapOutputHeapWithMetadataHeap.class, MapOutputCollector.class);

        final Job job = new Job(conf, "word count");
        job.setJarByClass(WordCount.class);

        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        // force a single reducer
        job.setNumReduceTasks(1);
        // force a single split
        FileInputFormat.setMinInputSplitSize(job, Long.MAX_VALUE);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        if (otherArgs.length > 2 && otherArgs[2].equals(KEYPREFIX)) {
            job.setSortComparatorClass(Comparator.class);
        }

        final Path input = new Path(otherArgs[0]);
        FileInputFormat.addInputPath(job, input);

        FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

        final FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(new Path(otherArgs[1]))) {
            hdfs.delete(new Path(otherArgs[1]), true);
        }

        final long start = System.nanoTime();
        final int r = job.waitForCompletion(true) ? 0 : 1;
        final long span = System.nanoTime() - start;
        System.out.println(TimeUnit.SECONDS.convert(span, TimeUnit.NANOSECONDS));
        System.out.println("======================================================");
        return r;
    }
}
