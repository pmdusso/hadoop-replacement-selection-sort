Hadoop Replacement Selection Sort

Pedro Martins Dusso, 20.04.2014
Tecnische Universität Kaiserslautern

This project provides an alternative sort algorithm for the sort phase in a Map task, namely replacement selection. Replacement selection is based on the observation that, if we save the value of the last key output, we can easily decide whether an incoming record can still be made part of the current run or has to be deferred to the next run.

The current project is designed to work with Hadoop  > 2.3, where the MAPREDUCE-2454 JIRA ("Allow external sorter plugin for MR") is available.